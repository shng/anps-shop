package com.kld.cms.api;


import com.kld.cms.po.SysArticle;

import com.kld.common.framework.dto.ResultMsg;
import com.kld.cms.po.SysArticle;
import java.util.List;

/**
 * hejinping 2016.3.24
 */
public interface ISysArticleService {

    int insertSelective(SysArticle record);

    int updateByPrimaryKeySelective(SysArticle record);

    int deleteArticlesByIds(List<Integer> list);

    ResultMsg getArticleList(int pageNum,int pageSize);

    /**
     * 查询前端最新文章列表或所有文章列表
     * @param num
     * @return
     */
    List<SysArticle> getArtWebList(Integer num);

    /**
     * 上面的那个，分页版本
     * @param pageNum
     * @param pageSize
     * @return
     */
    ResultMsg getArtWebList(int pageNum,int pageSize);

    /**
     * 根据ID查询文章详情
     * @param id
     * @return
     */
    SysArticle selectByPrimaryKey(Integer id);

}
