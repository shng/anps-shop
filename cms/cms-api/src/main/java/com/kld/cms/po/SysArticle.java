package com.kld.cms.po;
import java.io.Serializable;
import java.util.Date;

/**
 * hejinping 2016.3.24
 */
public class SysArticle implements Serializable {
    private static final long serialVersionUID = -1648397444712272292L;
    private Integer id;

    private String title;

    private Date publishtime;

    private String content;

    private Integer state;

    private Date createtime;

    private String creater;

    private String oucode;

    private String url;

    private Integer type;

    private Integer isdel;

    private String bsnsid;  //业务单号

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Date getPublishtime() {
        return publishtime;
    }

    public void setPublishtime(Date publishtime) {
        this.publishtime = publishtime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater == null ? null : creater.trim();
    }

    public String getOucode() {
        return oucode;
    }

    public void setOucode(String oucode) {
        this.oucode = oucode == null ? null : oucode.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIsdel() {
        return isdel;
    }

    public void setIsdel(Integer isdel) {
        this.isdel = isdel;
    }

    public String getBsnsid() {
        return bsnsid;
    }

    public void setBsnsid(String bsnsid) {
        this.bsnsid = bsnsid;
    }
}