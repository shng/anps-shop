/**
 * 58.com Inc.
 * Copyright (c) 2005-2015 All Rights Reserved.
 */
package com.kld.order.service.impl;

import com.kld.order.api.ICartService;
import com.kld.order.dao.CartDao;
import com.kld.order.po.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author yangjian
 * @version $Id: CartServiceImpl.java, v 0.1 2015-8-9 下午5:17:32 yangjian Exp $
 */
@Service("CartService")
public class CartServiceImpl implements ICartService {


}
