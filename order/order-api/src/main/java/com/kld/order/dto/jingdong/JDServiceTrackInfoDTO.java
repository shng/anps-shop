package com.kld.order.dto.jingdong;

/**
 * Created by Administrator on 2015/8/12.
 */
public class JDServiceTrackInfoDTO {

    public Integer afsServiceId;//服务单号
    public String title;//追踪标题
    public String context;//追踪内容
    public String createDate;//提交时间 格式 yyyy-MM-dd HH:mm:ss
    public String createName;//操作人昵称
    public String createPin;//操作人帐号

    public Integer getAfsServiceId() {
        return afsServiceId;
    }

    public void setAfsServiceId(Integer afsServiceId) {
        this.afsServiceId = afsServiceId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getCreatePin() {
        return createPin;
    }

    public void setCreatePin(String createPin) {
        this.createPin = createPin;
    }
}
