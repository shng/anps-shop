package com.kld.order.dto;

import java.math.BigDecimal;

/**
 * Created by Administrator on 2015/8/28.
 */
public class PartnerTableVO {

    private String acceptName;
    private String tradeType;//交易类型
    private String merchantName;//合作商名
    private String num;//数量
    private String tax;//税率
    private String mctprice;//
    private String taxprice;
    private String selfprice;
    private String taxSelfprice;
    private String nakedselfprice;
    private String points;
    private BigDecimal state;

    public String getNakedselfprice() {
        return nakedselfprice;
    }

    public void setNakedselfprice(String nakedselfprice) {
        this.nakedselfprice = nakedselfprice;
    }

    public String getTaxSelfprice() {
        return taxSelfprice;
    }

    public void setTaxSelfprice(String taxSelfprice) {
        this.taxSelfprice = taxSelfprice;
    }

    public BigDecimal getState() {
        return state;
    }

    public void setState(BigDecimal state) {
        this.state = state;
    }

    public String getAcceptName() {
        return acceptName;
    }

    public void setAcceptName(String acceptName) {
        this.acceptName = acceptName;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getMctprice() {
        return mctprice;
    }

    public void setMctprice(String mctprice) {
        this.mctprice = mctprice;
    }

    public String getTaxprice() {
        return taxprice;
    }

    public void setTaxprice(String taxprice) {
        this.taxprice = taxprice;
    }

    public String getSelfprice() {
        return selfprice;
    }

    public void setSelfprice(String selfprice) {
        this.selfprice = selfprice;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
