package com.kld.order.dto.jingdong;

import java.util.List;

/**
 * Created by Administrator on 2015/8/12.
 */
public class JDCompatibleServiceDetailDTO {

    public Integer afsServiceId; //服务单号
    public Integer customerExpect; //服务类型码 退货(10)、换货(20)、维修(30)
    public String afsApplyTime; //服务单申请时间  格式 yyyy-MM-dd HH:mm:ss
    public Long orderId; //订单号
    public int isHasInvoice; //是否有发票 0没有 1有
    public int isNeedDetectionReport; //是否有检测报告 0没有 1有
    public int isHasPackage; //是否有包装 0没有 1有
    public String questionPic; // 上 传 图片 访问 地址  不同图片逗号分割，可能为空
    public int afsServiceStep; //服务单环节 申请阶段(10),审核不通过(20),客服审核(21),商家审核 (22),京东收货(31), 商家收货(32), 京东处理(33),商家处理(34), 用户确认 (40),完成(50), 取消(60)
    public String afsServiceStepName; //服务单环节名称 申请阶段,客服审核,商家审核,京东收货,商家收货,京东处理,商家处理,用户确认,完成, 取消;
    public String approveNotes;//审核意见
    public String questionDesc;// 问题描述 可能为空
    public Integer approvedResult;//审核结果  直赔积分 (11),直赔余额(12),直赔优惠卷(13), 直 赔 京 豆(14),直赔商品(21),上门换新  (22), 自营取件(31), 客 户 送 货(32), 客 户 发 货(33), 闪 电 退 款(34), 虚 拟 退 款(35),大家电检测(80),大家电安装(81),大家电移机(82),大家电维修(83),大家电其它(84);
    public String approvedResultName;//审核结果名称 直赔积分,,直赔余额,,直赔优惠卷,直赔京豆, 直 赔 商 品,上门换新,自营取件 ,客 户 送 货,客户发货,闪 电 退 款, 虚 拟 退 款,大家电检测,大 家 电安装,大家电移机,大家电维修 ,大家电其它;
    public Integer processResult;//处理结果 返修换新(23),退货(40),换良(50),原返 60),病单  (71),出检(72),维修(73),强制关单(80),线下换新(90)
    public String processResultName; //处理结果名称  返修换新,退货 , 换良,原返,病单,出检,维修,强制关单,线下换新
    public JDServiceCustomerInfoDTO serviceCustomerInfoDTO; //客户信息
    public JDServiceAftersalesAddressInfoDTO serviceAftersalesAddressInfoDTO;//售后地址信息
    public JDServiceExpressInfoDTO serviceExpressInfoDTO;//客户发货信息
    public List<JDServiceFinanceDetailInfoDTO> serviceFinanceDetailInfoDTOs ;//退款明细
    public List<JDServiceTrackInfoDTO> serviceTrackInfoDTOs; //服务单追踪信息
    public List<JDServiceDetailInfoDTO> serviceDetailInfoDTOs;//服务单商品明细
    public List<Integer> allowOperations;//获取服务单允许的操作列表 列表为空代表不允许操作 列表包含1代表取消  列表包含2代表允许填写或者修改客户发货信息

    public Integer getAfsServiceId() {
        return afsServiceId;
    }

    public void setAfsServiceId(Integer afsServiceId) {
        this.afsServiceId = afsServiceId;
    }

    public Integer getCustomerExpect() {
        return customerExpect;
    }

    public void setCustomerExpect(Integer customerExpect) {
        this.customerExpect = customerExpect;
    }

    public String getAfsApplyTime() {
        return afsApplyTime;
    }

    public void setAfsApplyTime(String afsApplyTime) {
        this.afsApplyTime = afsApplyTime;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public int getIsHasInvoice() {
        return isHasInvoice;
    }

    public void setIsHasInvoice(int isHasInvoice) {
        this.isHasInvoice = isHasInvoice;
    }

    public int getIsNeedDetectionReport() {
        return isNeedDetectionReport;
    }

    public void setIsNeedDetectionReport(int isNeedDetectionReport) {
        this.isNeedDetectionReport = isNeedDetectionReport;
    }

    public int getIsHasPackage() {
        return isHasPackage;
    }

    public void setIsHasPackage(int isHasPackage) {
        this.isHasPackage = isHasPackage;
    }

    public String getQuestionPic() {
        return questionPic;
    }

    public void setQuestionPic(String questionPic) {
        this.questionPic = questionPic;
    }

    public int getAfsServiceStep() {
        return afsServiceStep;
    }

    public void setAfsServiceStep(int afsServiceStep) {
        this.afsServiceStep = afsServiceStep;
    }

    public String getAfsServiceStepName() {
        return afsServiceStepName;
    }

    public void setAfsServiceStepName(String afsServiceStepName) {
        this.afsServiceStepName = afsServiceStepName;
    }

    public String getApproveNotes() {
        return approveNotes;
    }

    public void setApproveNotes(String approveNotes) {
        this.approveNotes = approveNotes;
    }

    public String getQuestionDesc() {
        return questionDesc;
    }

    public void setQuestionDesc(String questionDesc) {
        this.questionDesc = questionDesc;
    }

    public Integer getApprovedResult() {
        return approvedResult;
    }

    public void setApprovedResult(Integer approvedResult) {
        this.approvedResult = approvedResult;
    }

    public String getApprovedResultName() {
        return approvedResultName;
    }

    public void setApprovedResultName(String approvedResultName) {
        this.approvedResultName = approvedResultName;
    }

    public Integer getProcessResult() {
        return processResult;
    }

    public void setProcessResult(Integer processResult) {
        this.processResult = processResult;
    }

    public String getProcessResultName() {
        return processResultName;
    }

    public void setProcessResultName(String processResultName) {
        this.processResultName = processResultName;
    }

    public JDServiceCustomerInfoDTO getServiceCustomerInfoDTO() {
        return serviceCustomerInfoDTO;
    }

    public void setServiceCustomerInfoDTO(JDServiceCustomerInfoDTO serviceCustomerInfoDTO) {
        this.serviceCustomerInfoDTO = serviceCustomerInfoDTO;
    }

    public JDServiceAftersalesAddressInfoDTO getServiceAftersalesAddressInfoDTO() {
        return serviceAftersalesAddressInfoDTO;
    }

    public void setServiceAftersalesAddressInfoDTO(JDServiceAftersalesAddressInfoDTO serviceAftersalesAddressInfoDTO) {
        this.serviceAftersalesAddressInfoDTO = serviceAftersalesAddressInfoDTO;
    }

    public JDServiceExpressInfoDTO getServiceExpressInfoDTO() {
        return serviceExpressInfoDTO;
    }

    public void setServiceExpressInfoDTO(JDServiceExpressInfoDTO serviceExpressInfoDTO) {
        this.serviceExpressInfoDTO = serviceExpressInfoDTO;
    }

    public List<JDServiceFinanceDetailInfoDTO> getServiceFinanceDetailInfoDTOs() {
        return serviceFinanceDetailInfoDTOs;
    }

    public void setServiceFinanceDetailInfoDTOs(List<JDServiceFinanceDetailInfoDTO> serviceFinanceDetailInfoDTOs) {
        this.serviceFinanceDetailInfoDTOs = serviceFinanceDetailInfoDTOs;
    }

    public List<JDServiceTrackInfoDTO> getServiceTrackInfoDTOs() {
        return serviceTrackInfoDTOs;
    }

    public void setServiceTrackInfoDTOs(List<JDServiceTrackInfoDTO> serviceTrackInfoDTOs) {
        this.serviceTrackInfoDTOs = serviceTrackInfoDTOs;
    }

    public List<JDServiceDetailInfoDTO> getServiceDetailInfoDTOs() {
        return serviceDetailInfoDTOs;
    }

    public void setServiceDetailInfoDTOs(List<JDServiceDetailInfoDTO> serviceDetailInfoDTOs) {
        this.serviceDetailInfoDTOs = serviceDetailInfoDTOs;
    }

    public List<Integer> getAllowOperations() {
        return allowOperations;
    }

    public void setAllowOperations(List<Integer> allowOperations) {
        this.allowOperations = allowOperations;
    }
}
