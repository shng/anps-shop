package com.kld.order.dto.jingdong;

/**
 * Created by Administrator on 2015/8/12.
 */
public class JDComponentExport {

    public String code;//服务类型码  上门取件(4)、客户发货(40)、客户送货(7)  退货(10)、换货(20)、 维修(30)
    public String name;//服务类型名称 上门取件、客户发货、客户送货, 退货、换货、维修

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
