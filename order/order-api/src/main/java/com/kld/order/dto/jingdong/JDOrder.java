package com.kld.order.dto.jingdong;

/**
 * Created by Administrator on 2015/7/24.
 */
public class JDOrder {

    public String getThirdOrder() {
        return thirdOrder;
    }

    public void setThirdOrder(String thirdOrder) {
        this.thirdOrder = thirdOrder;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProvince() {
        return province;
    }

    public void setProvince(int province) {
        this.province = province;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public int getCounty() {
        return county;
    }

    public void setCounty(int county) {
        this.county = county;
    }

    public int getTown() {
        return town;
    }

    public void setTown(int town) {
        this.town = town;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getInvoiceState() {
        return invoiceState;
    }

    public void setInvoiceState(int invoiceState) {
        this.invoiceState = invoiceState;
    }

    public int getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(int invoiceType) {
        this.invoiceType = invoiceType;
    }

    public int getSelectedInvoiceTitle() {
        return selectedInvoiceTitle;
    }

    public void setSelectedInvoiceTitle(int selectedInvoiceTitle) {
        this.selectedInvoiceTitle = selectedInvoiceTitle;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getInvoiceContent() {
        return invoiceContent;
    }

    public void setInvoiceContent(int invoiceContent) {
        this.invoiceContent = invoiceContent;
    }

    public int getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(int paymentType) {
        this.paymentType = paymentType;
    }

    public int getSubmitState() {
        return submitState;
    }

    public void setSubmitState(int submitState) {
        this.submitState = submitState;
    }

    public String getInvoiceName() {
        return invoiceName;
    }

    public void setInvoiceName(String invoiceName) {
        this.invoiceName = invoiceName;
    }

    public String getInvoicePhone() {

        return invoicePhone;
    }

    public void setInvoicePhone(String invoicePhone) {
        this.invoicePhone = invoicePhone;
    }

    public String getInvoiceProvice() {
        return invoiceProvice;
    }

    public void setInvoiceProvice(String invoiceProvice) {
        this.invoiceProvice = invoiceProvice;
    }

    public String getInvoiceCity() {
        return invoiceCity;
    }

    public void setInvoiceCity(String invoiceCity) {
        this.invoiceCity = invoiceCity;
    }

    public String getInvoiceCounty() {
        return invoiceCounty;
    }

    public void setInvoiceCounty(String invoiceCounty) {
        this.invoiceCounty = invoiceCounty;
    }

    public String getInvoiceAddress() {
        return invoiceAddress;
    }

    public void setInvoiceAddress(String invoiceAddress) {
        this.invoiceAddress = invoiceAddress;
    }

    String thirdOrder;  //第三方的订单单号
    String sku; //[{"skuId":商品编号, "num":商品数量}](最高支持50种商品)
    String name; //收货人
    int province; //一级地址
    int city; //二级地址
    int county; //三级地址
    int town; //四级地址(如果该地区有四级地址，则必须传递四级地址)
    String address; //详细地址
    String zip; //邮编
    String phone; //座机号 (与mobile其中一个有值即可)
    String mobile; //手机号 （与phone其中一个有值即可）
    String email; //邮箱
    String remark; //备注（少于100字）
    int invoiceState; //开票方式(1为随货开票，0为订单预借，2为集中开票 )
    int invoiceType; //1普通发票2增值税发票
    int selectedInvoiceTitle; //4个人，5单位
    String companyName; //发票抬头  (如果selectedInvoiceTitle=5则此字段必须)
    int invoiceContent; //1:明细，3：电脑配件，19:耗材，22：办公用品     备注:若增值发票则只能选1 明细
    int paymentType; //1：货到付款
    int submitState; //是否预占库存，0是预占库存(需要调用确认订单接口)，1是不预占库存
    String invoiceName; //增值票收票人姓名    备注：当invoiceType=2 且invoiceState=1时则此字段必填
    String invoicePhone; //增值票收票人电话   备注：当invoiceType=2 且invoiceState=1时则此字段必填
    String invoiceProvice; //增值票收票人所在省    备注：当invoiceType=2 且invoiceState=1时则此字段必填
    String invoiceCity; //增值票收票人所在市    备注：当invoiceType=2 且invoiceState=1时则此字段必填
    String invoiceCounty; // 增值票收票人所在区/县    备注：当invoiceType=2 且invoiceState=1时则此字段必填
    String invoiceAddress ; // 增值票收票人所在地址    备注：当invoiceType=2 且invoiceState=1时则此字段必填



}
