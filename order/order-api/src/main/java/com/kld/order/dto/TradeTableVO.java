package com.kld.order.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jw on 2015/8/27.
 */
public class TradeTableVO {
    private Integer id;
    private String catecode;

    private String categoryName;
    private String categoryName2;//2级分类名称
    private String categoryName3;//1级分类名称
    private String name;
    private String code;
    private String merchantcode;
    private String merchantname;//供应商名称
    private Date createtime;
    private String creator;
    private String modifier;
    private Date moditime;
    private String picurl;
    private int state;
    private String thirdcode;//第三方商品编号
    private int isdel; //是否删除

    private int suggestpoints;
    private double tax; //税率
    private String approver;
    private Date approvetime;
    private String oucode;
    private BigDecimal approvepoints;
    private int approvalstate;//审核状态

    private double sprice;//兑换收入
    private double mctprice; //兑换成本
    private Integer points;//兑换积分
    private double selfprice;//兑换金额
    private double mle;//毛利额
    private double mll;//毛利率
    private BigDecimal num;//兑换 个数


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCatecode() {
        return catecode;
    }

    public void setCatecode(String catecode) {
        this.catecode = catecode;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName2() {
        return categoryName2;
    }

    public void setCategoryName2(String categoryName2) {
        this.categoryName2 = categoryName2;
    }

    public String getCategoryName3() {
        return categoryName3;
    }

    public void setCategoryName3(String categoryName3) {
        this.categoryName3 = categoryName3;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMerchantcode() {
        return merchantcode;
    }

    public void setMerchantcode(String merchantcode) {
        this.merchantcode = merchantcode;
    }

    public String getMerchantname() {
        return merchantname;
    }

    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public Date getModitime() {
        return moditime;
    }

    public void setModitime(Date moditime) {
        this.moditime = moditime;
    }

    public String getPicurl() {
        return picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getThirdcode() {
        return thirdcode;
    }

    public void setThirdcode(String thirdcode) {
        this.thirdcode = thirdcode;
    }

    public int getIsdel() {
        return isdel;
    }

    public void setIsdel(int isdel) {
        this.isdel = isdel;
    }

    public int getSuggestpoints() {
        return suggestpoints;
    }

    public void setSuggestpoints(int suggestpoints) {
        this.suggestpoints = suggestpoints;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public Date getApprovetime() {
        return approvetime;
    }

    public void setApprovetime(Date approvetime) {
        this.approvetime = approvetime;
    }

    public String getOucode() {
        return oucode;
    }

    public void setOucode(String oucode) {
        this.oucode = oucode;
    }

    public BigDecimal getApprovepoints() {
        return approvepoints;
    }

    public void setApprovepoints(BigDecimal approvepoints) {
        this.approvepoints = approvepoints;
    }

    public int getApprovalstate() {
        return approvalstate;
    }

    public void setApprovalstate(int approvalstate) {
        this.approvalstate = approvalstate;
    }

    public double getSprice() {
        return sprice;
    }

    public void setSprice(double sprice) {
        this.sprice = sprice;
    }

    public double getMctprice() {
        return mctprice;
    }

    public void setMctprice(double mctprice) {
        this.mctprice = mctprice;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public double getSelfprice() {
        return selfprice;
    }

    public void setSelfprice(double selfprice) {
        this.selfprice = selfprice;
    }

    public double getMle() {
        return mle;
    }

    public void setMle(double mle) {
        this.mle = mle;
    }

    public double getMll() {
        return mll;
    }

    public void setMll(double mll) {
        this.mll = mll;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }
}
