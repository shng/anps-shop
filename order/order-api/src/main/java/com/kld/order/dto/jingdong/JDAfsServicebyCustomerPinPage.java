package com.kld.order.dto.jingdong;

import java.util.List;

/**
 * Created by Administrator on 2015/8/12.
 */
public class JDAfsServicebyCustomerPinPage {

    public List<JDAfsServicebyCustomerPin> serviceInfoList;
    public int totalNum; //总记录数
    public int pageSize; //每页记录数
    public int pageNum; //总页数
    public int pageIndex; //当前页数

    public List<JDAfsServicebyCustomerPin> getServiceInfoList() {
        return serviceInfoList;
    }

    public void setServiceInfoList(List<JDAfsServicebyCustomerPin> serviceInfoList) {
        this.serviceInfoList = serviceInfoList;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }
}
