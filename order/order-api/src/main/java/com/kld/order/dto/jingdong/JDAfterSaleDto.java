package com.kld.order.dto.jingdong;

/**
 * Created by Administrator on 2015/8/12.
 */
public class JDAfterSaleDto {

    public Long jdOrderId; //京东订单号
    public Integer customerExpect;//客户预期 必填，退货(10)、换货(20)、维修(30)
    public String questionDesc; //产品问题描述 最多1000字符
    public boolean isNeedDetectionReport;//是否需要检测报告
    public String questionPic;//问题描述图片 最多2000字符
    public boolean isHasPackage;//是否有包装
    public Integer packageDesc;//包装描述 0 无包装 10 包装完整  20 包装破损
    public JDAfterSaleCustomerDto asCustomerDto;//客户信息实体 必填
    public  JDAfterSalePickwareDto asPickwareDto;//取件信息实体 必填
    public JDAfterSaleReturnwareDto asReturnwareDto;//返件信息实体 必填
    public JDAfterSaleDetailDto asDetailDto;//申请单明细 必填

    public Long getJdOrderId() {
        return jdOrderId;
    }

    public void setJdOrderId(Long jdOrderId) {
        this.jdOrderId = jdOrderId;
    }

    public Integer getCustomerExpect() {
        return customerExpect;
    }

    public void setCustomerExpect(Integer customerExpect) {
        this.customerExpect = customerExpect;
    }

    public String getQuestionDesc() {
        return questionDesc;
    }

    public void setQuestionDesc(String questionDesc) {
        this.questionDesc = questionDesc;
    }

    public boolean isNeedDetectionReport() {
        return isNeedDetectionReport;
    }

    public void setIsNeedDetectionReport(boolean isNeedDetectionReport) {
        this.isNeedDetectionReport = isNeedDetectionReport;
    }

    public String getQuestionPic() {
        return questionPic;
    }

    public void setQuestionPic(String questionPic) {
        this.questionPic = questionPic;
    }

    public boolean isHasPackage() {
        return isHasPackage;
    }

    public void setIsHasPackage(boolean isHasPackage) {
        this.isHasPackage = isHasPackage;
    }

    public Integer getPackageDesc() {
        return packageDesc;
    }

    public void setPackageDesc(Integer packageDesc) {
        this.packageDesc = packageDesc;
    }

    public JDAfterSaleCustomerDto getAsCustomerDto() {
        return asCustomerDto;
    }

    public void setAsCustomerDto(JDAfterSaleCustomerDto asCustomerDto) {
        this.asCustomerDto = asCustomerDto;
    }

    public JDAfterSalePickwareDto getAsPickwareDto() {
        return asPickwareDto;
    }

    public void setAsPickwareDto(JDAfterSalePickwareDto asPickwareDto) {
        this.asPickwareDto = asPickwareDto;
    }

    public JDAfterSaleReturnwareDto getAsReturnwareDto() {
        return asReturnwareDto;
    }

    public void setAsReturnwareDto(JDAfterSaleReturnwareDto asReturnwareDto) {
        this.asReturnwareDto = asReturnwareDto;
    }

    public JDAfterSaleDetailDto getAsDetailDto() {
        return asDetailDto;
    }

    public void setAsDetailDto(JDAfterSaleDetailDto asDetailDto) {
        this.asDetailDto = asDetailDto;
    }
}

