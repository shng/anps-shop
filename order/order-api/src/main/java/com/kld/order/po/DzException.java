package com.kld.order.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DzException implements Serializable {
    private static final long serialVersionUID = 5133614305804304395L;

    private BigDecimal id;
    private String orderid;
    private String cardno;
    private Integer exceptioncase;
    private Integer orderstate;
    private String processtype;
    private Integer processstate;
    private String processmemo;
    private Date createtime;
    private Integer points;
    private String merchantcode;
    private String merchantname;
    private String fromwhere;
    private String fromaccount;
    private Date ordertime;
    private Integer ordertype;  //订单类型（1商品兑换；2积分抽奖）

    private BigDecimal aid;

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public Integer getExceptioncase() {
        return exceptioncase;
    }

    public void setExceptioncase(Integer exceptioncase) {
        this.exceptioncase = exceptioncase;
    }

    public Integer getOrderstate() {
        return orderstate;
    }

    public void setOrderstate(Integer orderstate) {
        this.orderstate = orderstate;
    }

    public String getProcesstype() {
        return processtype;
    }

    public void setProcesstype(String processtype) {
        this.processtype = processtype;
    }

    public Integer getProcessstate() {
        return processstate;
    }

    public void setProcessstate(Integer processstate) {
        this.processstate = processstate;
    }

    public String getProcessmemo() {
        return processmemo;
    }

    public void setProcessmemo(String processmemo) {
        this.processmemo = processmemo;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getMerchantcode() {
        return merchantcode;
    }

    public void setMerchantcode(String merchantcode) {
        this.merchantcode = merchantcode;
    }

    public String getMerchantname() {
        return merchantname;
    }

    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    public String getFromwhere() {
        return fromwhere;
    }

    public void setFromwhere(String fromwhere) {
        this.fromwhere = fromwhere;
    }

    public String getFromaccount() {
        return fromaccount;
    }

    public void setFromaccount(String fromaccount) {
        this.fromaccount = fromaccount;
    }

    public Date getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(Date ordertime) {
        this.ordertime = ordertime;
    }

    public BigDecimal getAid() {
        return aid;
    }

    public void setAid(BigDecimal aid) {
        this.aid = aid;
    }

    public Integer getOrdertype() {
        return ordertype;
    }

    public void setOrdertype(Integer ordertype) {
        this.ordertype = ordertype;
    }
}