package com.kld.order.dto.jingdong;

import java.math.BigDecimal;

/**
 * Created by Administrator on 2015/8/12.
 */
public class JDServiceFinanceDetailInfoDTO {

    public int refundWay;//退款方式
    public String refundWayName;//退款方式名称
    public int status;//状态
    public String statusName;//状态名称
    public BigDecimal refundPrice;//退款金额
    public String wareName;//商品名称
    public Integer wareId;//商品编号

    public int getRefundWay() {
        return refundWay;
    }

    public void setRefundWay(int refundWay) {
        this.refundWay = refundWay;
    }

    public String getRefundWayName() {
        return refundWayName;
    }

    public void setRefundWayName(String refundWayName) {
        this.refundWayName = refundWayName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public BigDecimal getRefundPrice() {
        return refundPrice;
    }

    public void setRefundPrice(BigDecimal refundPrice) {
        this.refundPrice = refundPrice;
    }

    public String getWareName() {
        return wareName;
    }

    public void setWareName(String wareName) {
        this.wareName = wareName;
    }

    public Integer getWareId() {
        return wareId;
    }

    public void setWareId(Integer wareId) {
        this.wareId = wareId;
    }
}
