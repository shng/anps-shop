package com.kld.order.dto;

import com.kld.promotion.po.Actlog;

import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Created by Dan on 2016/1/18.
 */
public class DailyDetailRptVO {
    private Date rptdate;
    private Integer num;
    private Integer points;
    private Double amount;
    private Double nakedamount;
    private Integer actnum;
    private Integer actpoints;
    private Double actamount;
    private Double actnakedamount;
    private Integer snum;
    private Integer spoints;
    private Double samount; //销售收入（不含税）
    private Double snakedamount;  //销售收入（不含税）
    private Map<Integer,Actlog> winpacts;//多种的活动
    private Integer swinnum;
    private Double freight;//运费

    public Double getFreight() {
        return freight != null?freight:0d;
    }

    public void setFreight(Double freight) {
        this.freight = freight;
    }

    public Double getNakedamount() {
        return nakedamount==null?0:nakedamount;
    }

    public void setNakedamount(Double nakedamount) {
        this.nakedamount = nakedamount;
    }

    public Double getActnakedamount() {
        return actnakedamount==null?0:actnakedamount;
    }

    public void setActnakedamount(Double actnakedamount) {
        this.actnakedamount = actnakedamount;
    }

    public Date getRptdate() {
        return rptdate;
    }

    public void setRptdate(Date rptdate) {
        this.rptdate = rptdate;
    }

    public Integer getNum() {
        return num==null?0:num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getPoints() {
        return points==null?0:points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Double getAmount() {
        return amount ==null?0: amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getActnum() {
        return actnum==null?0:actnum;
    }

    public void setActnum(Integer actnum) {
        this.actnum = actnum;
    }

    public Integer getActpoints() {
        return actpoints==null?0:actpoints;
    }

    public void setActpoints(Integer actpoints) {
        this.actpoints = actpoints;
    }

    public Double getActamount() {
        return actamount ==null?0: actamount;
    }

    public void setActamount(Double actamount) {
        this.actamount = actamount;
    }

    public Integer getSnum() {
        snum = getNum()+getActnum();
        return snum;
    }

    public Integer getSpoints() {
        spoints=getPoints()+getActpoints();
        return spoints;
    }

    public Double getSamount() {
        samount = getAmount()+ getActamount();
        return samount;
    }

    public Double getSnakedamount() {
        snakedamount = getNakedamount()+getActnakedamount();
        return snakedamount==null?0:snakedamount;
    }


    public Map<Integer, Actlog> getWinpacts() {
        return winpacts;
    }

    public void setWinpacts(Map<Integer, Actlog> winpacts) {
        this.winpacts = winpacts;
    }

    public Integer getSwinnum() {
        swinnum=0;
        Set<Integer> keys = winpacts.keySet();
        if(winpacts!=null && keys.size()>0){
            for(Integer k:keys){
                swinnum+=winpacts.get(k).getTimes().intValue();
            }
        }
        return swinnum;
    }
}
