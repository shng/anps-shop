package com.kld.order.api;

/**
 * Created by Dan on 2015/11/26.
 */
public interface PointPriceService {
    public  Integer suggestPoints(Double price);
    public Double OperatingPrice(Integer points);
    public Double ConversionPoints(Double discountrate, Double mctprice);
}
