package com.kld.order.api;

import com.kld.order.po.SubOrder;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2015/8/15.
 */
public interface SubOrderService {

    int deleteSubOrderByThirdId(String merchant, String thirdid);

    int updateSubOrderStateByThirdId(String merchantcode, List<String> list, Integer state);

    int deleteByPrimaryKey(Integer id);

    int saveSubOrder(SubOrder subOrder);

    List<SubOrder> getAllRefuseOrders(Map<String, Object> map);

    int updateSubOrderById(Integer id, Integer state, Date datet);

    int updateSubOrderByThirdId(String merchant, String thirdid, Integer state, Date datet);

    SubOrder selectSubOrderByThirdId(String merchant, String thirdId);

    List<Integer> getSubOrderStateBySubOrderId(Integer id);

    SubOrder selectSuborderByOrderId(Integer orderId, String jdOrderId);

    int updateByPrimaryKeySelective(SubOrder subOrder);
    int updateMerchantPrice(SubOrder record);

    int insertSelective(SubOrder subOrder);

    List<SubOrder> getSubOrderWithItemsByOrderID(int orderid);

    /**
     * 合作商和积分商城对账
     * @param map
     * @return
     */
    List<SubOrder> getSuborderListBill(Map<String, Object> map);
    List<SubOrder> getSubOrderListForJDBill();

    List<SubOrder> getSubOrderList(Map<String, Object> map);
}
