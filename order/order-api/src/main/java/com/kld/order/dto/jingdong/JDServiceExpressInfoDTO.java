package com.kld.order.dto.jingdong;

/**
 * Created by Administrator on 2015/8/12.
 */
public class JDServiceExpressInfoDTO {

    public Integer afsServiceId;//服务单号
    public String freightMoney;//运费
    public String expressCompany;//快递公司名 中文
    public String deliverDate; //客户发货日期 格式 yyyy-MM-dd HH:mm:ss
    public String expressCode;//快递单号

    public Integer getAfsServiceId() {
        return afsServiceId;
    }

    public void setAfsServiceId(Integer afsServiceId) {
        this.afsServiceId = afsServiceId;
    }

    public String getFreightMoney() {
        return freightMoney;
    }

    public void setFreightMoney(String freightMoney) {
        this.freightMoney = freightMoney;
    }

    public String getExpressCompany() {
        return expressCompany;
    }

    public void setExpressCompany(String expressCompany) {
        this.expressCompany = expressCompany;
    }

    public String getDeliverDate() {
        return deliverDate;
    }

    public void setDeliverDate(String deliverDate) {
        this.deliverDate = deliverDate;
    }

    public String getExpressCode() {
        return expressCode;
    }

    public void setExpressCode(String expressCode) {
        this.expressCode = expressCode;
    }
}
