package com.kld.order.dto;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by Dan on 2016/2/25.
 */
public class HotPointsOrderRpt {
//    private BigDecimal minpoints;
//    private BigDecimal maxpoints;
    private String pointsscope;
    private Integer pronum;
    private BigDecimal tradenum;
    private BigDecimal tradenumrate;
    private BigDecimal tradepoints;
    private BigDecimal tradepointsrate;
    private BigDecimal totaltradepoints;
    private BigDecimal totaltradenum;

    public String getPointsscope() {
        return pointsscope;
    }

    public void setPointsscope(String pointsscope) {
        this.pointsscope = pointsscope;
    }

    public Integer getPronum() {
        return pronum;
    }

    public void setPronum(Integer pronum) {
        this.pronum = pronum;
    }

    public BigDecimal getTradenum() {
        return tradenum==null?BigDecimal.valueOf(0):tradenum;
    }

    public void setTradenum(BigDecimal tradenum) {
        this.tradenum = tradenum;
    }

    public BigDecimal getTradenumrate() {
        tradenumrate=BigDecimal.valueOf(0);
        if(totaltradenum.intValue()>0){
            tradenumrate=new BigDecimal(new DecimalFormat("#.##").format(tradenum.doubleValue()/totaltradenum.doubleValue()*100));
        }
        return tradenumrate;
    }

    public BigDecimal getTradepoints() {
        return tradepoints;
    }

    public void setTradepoints(BigDecimal tradepoints) {
        this.tradepoints = tradepoints;
    }

    public BigDecimal getTradepointsrate() {
        tradepointsrate=BigDecimal.valueOf(0);
        if(totaltradepoints.intValue()>0){
            tradepointsrate=new BigDecimal(new DecimalFormat("#.##").format(tradepoints.doubleValue()/totaltradepoints.doubleValue()*100));
        }
        return tradepointsrate;
    }

    public BigDecimal getTotaltradepoints() {
        return totaltradepoints==null?BigDecimal.valueOf(0):totaltradepoints;
    }

    public void setTotaltradepoints(BigDecimal totaltradepoints) {
        this.totaltradepoints = totaltradepoints;
    }

    public BigDecimal getTotaltradenum() {
        return totaltradenum==null?BigDecimal.valueOf(0):totaltradenum;
    }

    public void setTotaltradenum(BigDecimal totaltradenum) {
        this.totaltradenum = totaltradenum;
    }
}
