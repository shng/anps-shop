package com.kld.order.api;

import com.github.pagehelper.PageInfo;
import com.kld.order.po.DzException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by caozheng
 */
public interface DzExceptionService {

    int saveDzException(DzException dzException);

    DzException getDzExceptionByID(Integer id);

    List<DzException> getAllDzExceptioinList(Map<String, Object> map);
    PageInfo getAllDzExceptioinList(Map<String,Object> map, int PageNum, int PageSize);
    /**
     *  update DZEXCEPTION set processtype=#{1},processstate=#{3},processmemo=#{2}  where id =#{0}
     * @param id
     * @param pt
     * @param pm
     * @param ps
     * @return
     */
    int updateDzExceptionById(BigDecimal id, Integer pt, String pm, Integer ps);

    List<DzException> getAllDzApprovalExceptioinList(Map<String, Object> map);

    int updateDzExceptionByIds(Integer processstate, List<Integer> list);

    DzException getProcessTypeById(Integer id);

    DzException getDzExceptionByOrderId(String orderId, Integer ordertype);
}
