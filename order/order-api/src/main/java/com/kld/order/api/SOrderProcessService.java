package com.kld.order.api;

import com.kld.order.po.SOrderProcess;

import java.math.BigDecimal;

/**
 * Created by Administrator on 2015/8/15.
 */
public interface SOrderProcessService {

    int saveSOrderProcess(SOrderProcess sOrderProcess);

    int updateSorderProcessById(SOrderProcess sOrderProcess);

    SOrderProcess getSorderProcessByTypeAndOrderId(Integer type, BigDecimal orderId);

    SOrderProcess getSorderProcessBySid(BigDecimal sid);
}
