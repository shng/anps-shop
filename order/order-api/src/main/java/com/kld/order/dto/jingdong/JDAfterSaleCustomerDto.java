package com.kld.order.dto.jingdong;

/**
 * Created by Administrator on 2015/8/12.
 *
 * 客户信息实体
 */
public class JDAfterSaleCustomerDto {

    public String customerContactName;//联系人最多50字符，能获取到就必填
    public String customerTel; //联系电话 最多50字符，能获取到就必填
    public String customerMobilePhone;//手机号  最多50字符
    public String customerEmail;//E-mail   最多50字符
    public String customerPostcode;//邮编  最多20字符

    public String getCustomerContactName() {
        return customerContactName;
    }

    public void setCustomerContactName(String customerContactName) {
        this.customerContactName = customerContactName;
    }

    public String getCustomerTel() {
        return customerTel;
    }

    public void setCustomerTel(String customerTel) {
        this.customerTel = customerTel;
    }

    public String getCustomerMobilePhone() {
        return customerMobilePhone;
    }

    public void setCustomerMobilePhone(String customerMobilePhone) {
        this.customerMobilePhone = customerMobilePhone;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPostcode() {
        return customerPostcode;
    }

    public void setCustomerPostcode(String customerPostcode) {
        this.customerPostcode = customerPostcode;
    }
}
