package com.kld.product.po;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProCategory implements Serializable{
    private static final long serialVersionUID = 1373147306162884777L;
    /**
     * 类目ID
     */
    private Integer id;

    /**
     * 分类名称
     */
    private String cateName;

    /**
     * 分类父级编码
     */
    private Integer pId;


    /**
     * 分类微信图片路径
     */
    private String picImg;

    /**
     * 是否在首页显示：1-显示，0-不显示
     */
    private Integer reCommend;

    /**
     * 是否已删除(0未删除;1已删除)
     */
    private Integer isDel;

    /**
     * 说明
     */
    private String remark;

    /**
     * 分类级别
     */
    private Integer lev;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 是否有子节点，有1，没有0
     */
    private Integer hasChild;

    /**
     * 状态（1：上架 0：下架）
     */
    private Integer state;

    private String categoryState;

    /**
     * 排序（新增）
     */
    private Integer sort;

    private List<ProCategory> children;

    //品牌id（用于保存）
    private List<Integer> brandList ;
    //属性（用于保存）
    private List<Integer> propertyList ;
    //规格（用于保存）
    private List<Integer> specsList ;

    /***
     * 用于页面传值
     */
    private String specArray;

    private String brandArray;

    private String propertyArray;

    public List<Integer> getBrandList() {
        return brandList;
    }

    public void setBrandList(List<Integer> brandList) {
        this.brandList = brandList;
    }

    public List<Integer> getPropertyList() {
        return propertyList;
    }

    public void setPropertyList(List<Integer> propertyList) {
        this.propertyList = propertyList;
    }

    public List<Integer> getSpecsList() {
        return specsList;
    }

    public void setSpecsList(List<Integer> specsList) {
        this.specsList = specsList;
    }

    public String getCategoryState() {
        return categoryState;
    }


    public void setCategoryState(String categoryState) {
        this.categoryState = categoryState;
    }

    public String getSpecArray() {
        return specArray;
    }

    public void setSpecArray(String specArray) {
        this.specArray = specArray;
    }

    public String getBrandArray() {
        return brandArray;
    }

    public void setBrandArray(String brandArray) {
        this.brandArray = brandArray;
    }

    public String getPropertyArray() {
        return propertyArray;
    }

    public void setPropertyArray(String propertyArray) {
        this.propertyArray = propertyArray;
    }

    public List<ProCategory> getChildren() {
        return children;
    }

    public void setChildren(List<ProCategory> children) {
        this.children = children;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getCateName() {
        return cateName;
    }

    public void setCateName(String cateName) {
        this.cateName = cateName;
    }

    public Integer getpId() {
        return pId;
    }

    public void setpId(Integer pId) {
        this.pId = pId;
    }

    public String getPicImg() {
        return picImg;
    }

    public void setPicImg(String picImg) {
        this.picImg = picImg;
    }

    public Integer getReCommend() {
        return reCommend;
    }

    public void setReCommend(Integer reCommend) {
        this.reCommend = reCommend;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getLev() {
        return lev;
    }

    public void setLev(Integer lev) {
        this.lev = lev;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getHasChild() {
        return hasChild;
    }

    public void setHasChild(Integer hasChild) {
        this.hasChild = hasChild;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}