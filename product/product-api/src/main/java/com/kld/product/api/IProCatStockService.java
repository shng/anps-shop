package com.kld.product.api;

import com.kld.product.po.ProCatStock;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
public interface IProCatStockService {

    /***
     *  根据ID删除
     * @param id
     * @return
     * @throws Exception
     */
    int deleteByPrimaryKey(Integer id) throws Exception;

    /***
     * 新增
     * @param proCatStock
     * @return
     */
    int insert(ProCatStock proCatStock)throws Exception;

    /***
     * 根据ID查询
     * @param id
     * @return
     */
    ProCatStock selectByPrimaryKey(Integer id)throws Exception;

    /***
     * 修改
     * @param proCatStock
     * @return
     */
    int updateByPrimaryKey(ProCatStock proCatStock)throws Exception;

    /***
     * 查询集合
     * @param proCatStock
     * @return
     */
    List<ProCatStock> findProCatStockList(ProCatStock proCatStock)throws Exception;

}
