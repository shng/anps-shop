package com.kld.product.api;

import com.github.pagehelper.PageInfo;
import com.kld.product.dto.ProductDto;
import com.kld.product.po.ProProduct;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
public interface IProProductService {

    /***
     *  根据ID删除
     * @param id
     * @return
     * @throws Exception
     */
    int deleteByPrimaryKey(Integer id) throws Exception;

    /***
     * 新增
     * @param proProduct
     * @return
     */
    int insert(ProProduct proProduct)throws Exception;

    /***
     * 根据ID查询
     * @param id
     * @return
     */
    ProProduct selectByPrimaryKey(Integer id)throws Exception;

    /***
     * 修改
     * @param proProduct
     * @return
     */
    int updateByPrimaryKey(ProProduct proProduct)throws Exception;

    /***
     * 查询集合
     * @param proProduct
     * @return
     */
    List<ProProduct> findProProductList(ProProduct proProduct)throws Exception;

    /***
     * 查看三级分类是否被商品使用
     * @param cateId
     * @return
     */
    int findCategoryIsProductById(int cateId);

    int insertProduct(ProductDto productDto,String[] imgUrl)throws Exception;

    /**
     * 分页查询商品集合
     * @param pageNum
     * @param pageSize
     * @param proProduct
     * @return
     * @throws Exception
     */
    PageInfo findProProductListPage(int pageNum, int pageSize, ProProduct proProduct)throws Exception;

    /***
     * 编辑商品
     * @param productDto
     * @param imgUrl
     * @return
     * @throws Exception
     */
    int updateProById(ProductDto productDto, String[] imgUrl)throws Exception;
}
