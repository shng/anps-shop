package com.kld.product.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by anpushang on 2016/3/24.
 */
public class ProSkus implements Serializable{

    private static final long serialVersionUID = -6733710233894849627L;



    /**
     * 主键
     */
    private Integer id;

    /**
     * 商品id
     */
    private Integer proId;

    /**
     * 名称
     */
    private String skuName;

    /**
     * 状态 0-禁用  1-启用
     */
    private Integer state;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 操作人
     */
    private String creator;

    /**
     * 图片地址
     */
    private String imgUrl;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 展示价
     */
    private BigDecimal showPrice;

    /**
     * 成本价
     */
    private BigDecimal costPrice;

    /**
     * 运营价
     */
    private BigDecimal price;

    /**
     * 活动时原运营价
     */
    private BigDecimal originalPrice;

    /**
     * 建议积分
     */
    private Integer suggePoints;

    /**
     * 运营积分
     */
    private Integer operatePoints;

    /**
     * 比率
     */
    private BigDecimal rate;

    /**
     * 生效方式(1:立即生效，2：定时生效，有一个任务调度表)
     */
    private Integer takeMode;

    /**
     * 营销活动ID
     */
    private Integer markeId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProId() {
        return proId;
    }

    public void setProId(Integer proId) {
        this.proId = proId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public BigDecimal getShowPrice() {
        return showPrice;
    }

    public void setShowPrice(BigDecimal showPrice) {
        this.showPrice = showPrice;
    }

    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Integer getSuggePoints() {
        return suggePoints;
    }

    public void setSuggePoints(Integer suggePoints) {
        this.suggePoints = suggePoints;
    }

    public Integer getOperatePoints() {
        return operatePoints;
    }

    public void setOperatePoints(Integer operatePoints) {
        this.operatePoints = operatePoints;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public Integer getTakeMode() {
        return takeMode;
    }

    public void setTakeMode(Integer takeMode) {
        this.takeMode = takeMode;
    }

    public Integer getMarkeId() {
        return markeId;
    }

    public void setMarkeId(Integer markeId) {
        this.markeId = markeId;
    }
}
