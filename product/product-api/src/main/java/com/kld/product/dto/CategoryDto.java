package com.kld.product.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by anpushang on 2016/3/31.
 */
public class CategoryDto implements Serializable{
    private static final long serialVersionUID = -1711160911553974485L;
    /**
     * 类目ID
     */
    private Integer id;

    /**
     * 分类名称
     */
    private String cateName;

    /**
     * 分类父级编码
     */
    private Integer pId;


    /**
     * 分类微信图片路径
     */
    private String picImg;

    /**
     * 是否在首页显示：1-显示，0-不显示
     */
    private Integer reCommend;


    /**
     * 说明
     */
    private String remark;

    /**
     * 分类级别
     */
    private Integer lev;



    /**
     * 是否有子节点，有1，没有0
     */
    private Integer hasChild;

    /**
     * 状态（1：上架 0：下架）
     */
    private Integer cateState;

    private String categoryState;

    /**
     * 排序（新增）
     */
    private Integer sort;

    private List<CategoryDto> children;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCateName() {
        return cateName;
    }

    public void setCateName(String cateName) {
        this.cateName = cateName;
    }

    public Integer getpId() {
        return pId;
    }

    public void setpId(Integer pId) {
        this.pId = pId;
    }

    public String getPicImg() {
        return picImg;
    }

    public void setPicImg(String picImg) {
        this.picImg = picImg;
    }

    public Integer getReCommend() {
        return reCommend;
    }

    public void setReCommend(Integer reCommend) {
        this.reCommend = reCommend;
    }


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getLev() {
        return lev;
    }

    public void setLev(Integer lev) {
        this.lev = lev;
    }


    public Integer getHasChild() {
        return hasChild;
    }

    public void setHasChild(Integer hasChild) {
        this.hasChild = hasChild;
    }

    public Integer getCateState() {
        return cateState;
    }

    public void setCateState(Integer cateState) {
        this.cateState = cateState;
    }

    public String getCategoryState() {
        return categoryState;
    }

    public void setCategoryState(String categoryState) {
        this.categoryState = categoryState;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public List<CategoryDto> getChildren() {
        return children;
    }

    public void setChildren(List<CategoryDto> children) {
        this.children = children;
    }
}
