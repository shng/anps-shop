package com.kld.product.api;

import com.github.pagehelper.PageInfo;
import com.kld.product.po.ProCategory;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
public interface IProCategoryService {
    /***
     * 删除
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Integer id)throws Exception;
    /***
     * 新增
     * @param proCategory
     * @return
     */
    int insert(ProCategory proCategory)throws Exception;
    /***
     * 查询
     * @param id
     * @return
     */
    ProCategory selectByPrimaryKey(Integer id)throws Exception;

    /***
     * 修改
     * @param proCategory
     * @return
     */
    int updateByPrimaryKey(ProCategory proCategory)throws Exception;

    /***
     * 根据级别查询分类
     * @param proCategory
     * @return
     * @throws Exception
     */
    List<ProCategory> findCategoryListByLev(ProCategory proCategory)throws Exception;


    List<ProCategory> findCategoryChild(Integer id)throws Exception;

    /**
     * 查询集合
     * @param proCategory
     * @return
     */
    List<ProCategory> findProCategoryList(ProCategory proCategory)throws Exception;

    PageInfo findProCateGoryPageList(int pageNum, int pageSize, ProCategory proCategory) throws Exception;

    /***
     * 添加三级分类,关联 属性 规格 品牌
     * @param proCategory
     * @return
     */
    int insertChildCategory(ProCategory proCategory)throws Exception;

    /***
     * 修改三级分类，关联 属性 规格 品牌
     * @param proCategory
     * @return
     * @throws Exception
     */
    int updateChildCategory(ProCategory proCategory)throws Exception;

    /***
     * 上下架分类操作
     * @param proCategory
     * @return
     */
    int updateChangePutAway(ProCategory proCategory)throws Exception;

    int getCateChildById(int id);

    /**
     * 根据父级ID去查询子集分类信息
     * @param pId
     * @return
     * @throws Exception
     */
    List<ProCategory> selectCateByPid(Integer pId)throws Exception;
}
