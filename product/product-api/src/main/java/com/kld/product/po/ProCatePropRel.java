package com.kld.product.po;

import java.io.Serializable;
import java.util.Date;

public class ProCatePropRel implements Serializable{
    private static final long serialVersionUID = -7895066069780586423L;
    /**
     * 主键
     */
    private Integer id;

    /**
     * 类目ID
     */
    private Integer cateId;

    /**
     * 属性表ID
     */
    private Integer propId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 操作人
     */
    private String creator;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCateId() {
        return cateId;
    }

    public void setCateId(Integer cateId) {
        this.cateId = cateId;
    }

    public Integer getPropId() {
        return propId;
    }

    public void setPropId(Integer propId) {
        this.propId = propId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}