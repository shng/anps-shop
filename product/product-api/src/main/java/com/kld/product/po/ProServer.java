package com.kld.product.po;

import java.io.Serializable;
import java.util.Date;

public class ProServer implements Serializable{
    private static final long serialVersionUID = -7974632630061360241L;
    /**
     * 主键
     */
    private Integer id;

    /**
     * 商品ID
     */
    private Integer proId;

    /**
     * 服务ID
     */
    private Integer svcId;

    /**
     * 截至时间
     */
    private Date abortTime;

    /**
     * 服务类型(1:售后服务，2：配送方式,3:支付方式)
     */
    private Integer svcType;

    /**
     * 处理方式(1手动，2：自动)
     */
    private Integer processMode;

    /**
     * 处理最少时间
     */
    private Integer processTime;
    /**
     * 换货有效期， 天
     */
    private Integer hXInDate;
    /**
     * 退货有效期 天
     */
    private Integer tHInDate;
    /***
     * 维修有效期 从字典中获取code
     */
    private String mainTain;
    /**
     * 混合支付最少设置积分
     */
    private Integer leatInte;

    /**
     * 混合支付最多设置积分
     */
    private Integer maxInte;

    public Integer gethXInDate() {
        return hXInDate;
    }

    public void sethXInDate(Integer hXInDate) {
        this.hXInDate = hXInDate;
    }


    public String getMainTain() {
        return mainTain;
    }

    public void setMainTain(String mainTain) {
        this.mainTain = mainTain;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProId() {
        return proId;
    }

    public void setProId(Integer proId) {
        this.proId = proId;
    }

    public Integer getSvcId() {
        return svcId;
    }

    public void setSvcId(Integer svcId) {
        this.svcId = svcId;
    }

    public Date getAbortTime() {
        return abortTime;
    }

    public void setAbortTime(Date abortTime) {
        this.abortTime = abortTime;
    }

    public Integer getSvcType() {
        return svcType;
    }

    public void setSvcType(Integer svcType) {
        this.svcType = svcType;
    }

    public Integer getProcessMode() {
        return processMode;
    }

    public void setProcessMode(Integer processMode) {
        this.processMode = processMode;
    }

    public Integer getProcessTime() {
        return processTime;
    }

    public void setProcessTime(Integer processTime) {
        this.processTime = processTime;
    }

    public Integer gettHInDate() {
        return tHInDate;
    }

    public void settHInDate(Integer tHInDate) {
        this.tHInDate = tHInDate;
    }

    public Integer getLeatInte() {
        return leatInte;
    }

    public void setLeatInte(Integer leatInte) {
        this.leatInte = leatInte;
    }

    public Integer getMaxInte() {
        return maxInte;
    }

    public void setMaxInte(Integer maxInte) {
        this.maxInte = maxInte;
    }
}