package com.kld.product.po;

import java.io.Serializable;
import java.util.Date;

public class ProPropValueRel implements Serializable{
    private static final long serialVersionUID = -523568669374561380L;
    /**
     * 主键
     */
    private Integer id;

    /**
     * 属性表_主键
     */
    private Integer propId;

    /**
     * 商品ID
     */
    private Integer proId;

    /**
     * 属性值id
     */
    private Integer propValueId;

    /**
     * 自定义属性值
     */
    private String propValue;

    /**
     * 启用状态 0-禁用 1-启用
     */
    private Integer isUse;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 操作人
     */
    private String creator;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPropId() {
        return propId;
    }

    public void setPropId(Integer propId) {
        this.propId = propId;
    }

    public Integer getProId() {
        return proId;
    }

    public void setProId(Integer proId) {
        this.proId = proId;
    }

    public Integer getPropValueId() {
        return propValueId;
    }

    public void setPropValueId(Integer propValueId) {
        this.propValueId = propValueId;
    }

    public String getPropValue() {
        return propValue;
    }

    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

    public Integer getIsUse() {
        return isUse;
    }

    public void setIsUse(Integer isUse) {
        this.isUse = isUse;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}