package com.kld.product.po;

import java.io.Serializable;

public class ProRelation implements Serializable{
    private static final long serialVersionUID = -6392580842238156648L;
    /**
     * ID
     */
    private Integer id;

    /**
     * 主商品ID
     */
    private Integer masterId;

    /**
     * 从商品ID
     */
    private Integer slaveId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMasterId() {
        return masterId;
    }

    public void setMasterId(Integer masterId) {
        this.masterId = masterId;
    }

    public Integer getSlaveId() {
        return slaveId;
    }

    public void setSlaveId(Integer slaveId) {
        this.slaveId = slaveId;
    }
}