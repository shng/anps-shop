package com.kld.product.po;

import java.io.Serializable;

public class PropSkuWareRel implements Serializable {
    private static final long serialVersionUID = 7870102596575417193L;
    /**
     * ID
     */
    private Integer id;

    /**
     * SKUID
     */
    private Integer skuId;

    /**
     * 仓库ID
     */
    private Integer wareId;

    /**
     * 库存
     */
    private Integer stock;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public Integer getWareId() {
        return wareId;
    }

    public void setWareId(Integer wareId) {
        this.wareId = wareId;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }
}