package com.kld.product.dto;

import com.kld.product.po.ProProduct;
import com.kld.product.po.ProServer;

import java.io.Serializable;

/**
 * Created by anpushang on 2016/4/5.
 */
public class ProductDto implements Serializable{

    private ProProduct proProduct;

    private ProServer proServer;

    private Integer hxSvcId;

    private Integer txSvcId;

    private Integer wxSvcId;

    private Integer kdysSvcId;

    private Integer smztSvcId;


    public ProServer getProServer() {
        return proServer;
    }

    public void setProServer(ProServer proServer) {
        this.proServer = proServer;
    }

    public ProProduct getProProduct() {
        return proProduct;
    }

    public void setProProduct(ProProduct proProduct) {
        this.proProduct = proProduct;
    }

    public Integer getHxSvcId() {
        return hxSvcId;
    }

    public void setHxSvcId(Integer hxSvcId) {
        this.hxSvcId = hxSvcId;
    }

    public Integer getTxSvcId() {
        return txSvcId;
    }

    public void setTxSvcId(Integer txSvcId) {
        this.txSvcId = txSvcId;
    }

    public Integer getWxSvcId() {
        return wxSvcId;
    }

    public void setWxSvcId(Integer wxSvcId) {
        this.wxSvcId = wxSvcId;
    }

    public Integer getKdysSvcId() {
        return kdysSvcId;
    }

    public void setKdysSvcId(Integer kdysSvcId) {
        this.kdysSvcId = kdysSvcId;
    }

    public Integer getSmztSvcId() {
        return smztSvcId;
    }

    public void setSmztSvcId(Integer smztSvcId) {
        this.smztSvcId = smztSvcId;
    }
}
