package com.kld.product.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ProProduct implements Serializable{
    private static final long serialVersionUID = -7258301201977004491L;
    /**
     * 商品ID
     */
    private Integer id;

    /**
     * 类目ID
     */
    private Integer cateId;

    /**
     * 品牌ID
     */
    private Integer brandId;

    /**
     * 0下架,1上架
     */
    private Integer state;

    /**
     * 商品条形码
     */
    private String proCode;

    /**
     * 供应商商品编码
     */
    private String mctProCode;

    /**
     * 供应商Code
     */
    private String mctCode;

    /**
     * 商品名称.缓存数据. 一周从第三方刷新一次
     */
    private String proName;

    /**
     * 商品主图,和名称一起更新
     */
    private String picUrl;

    /**
     * 0未删除,1删除
     */
    private Integer isDel;

    /**
     * 添加商品的公司.后续卡核心结账用做收单单位.
     */
    private String ouCode;

    /**
     * 参数
     */
    private String param;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private String modifyTime;

    /**
     * 修改人
     */
    private String modifier;
    /**
     * 产地
     */
    private String area;

    /**
     * 重量
     */
    private String weight;

    /**
     * 重量单位
     */
    private String unit;

    /**
     * 库存管理方
     */
    private String stockManage;

    /**
     * 微信详情
     */
    private String wxDetail;

    /**
     * PC详情
     */
    private String pxDetail;

    /**
     * 虚拟商品库存
     */
    private Integer xnStock;

    /**
     * 进项税率
     */
    private BigDecimal jxTax;

    /**
     * 商品类型
     */
    private Integer proType;
    /**
     * 虚拟商品有效天数
     */
    private Integer inDates;
    /***
     * 虚拟商品有效期开始时间
     */
    private Date inStartDate;
    /**
     * 虚拟商品有效期结束时间
     */
    private Date inEndDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCateId() {
        return cateId;
    }

    public void setCateId(Integer cateId) {
        this.cateId = cateId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getProCode() {
        return proCode;
    }

    public void setProCode(String proCode) {
        this.proCode = proCode;
    }

    public String getMctProCode() {
        return mctProCode;
    }

    public void setMctProCode(String mctProCode) {
        this.mctProCode = mctProCode;
    }

    public String getMctCode() {
        return mctCode;
    }

    public void setMctCode(String mctCode) {
        this.mctCode = mctCode;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public String getOuCode() {
        return ouCode;
    }

    public void setOuCode(String ouCode) {
        this.ouCode = ouCode;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getStockManage() {
        return stockManage;
    }

    public void setStockManage(String stockManage) {
        this.stockManage = stockManage;
    }

    public String getWxDetail() {
        return wxDetail;
    }

    public void setWxDetail(String wxDetail) {
        this.wxDetail = wxDetail;
    }

    public String getPxDetail() {
        return pxDetail;
    }

    public void setPxDetail(String pxDetail) {
        this.pxDetail = pxDetail;
    }

    public Integer getXnStock() {
        return xnStock;
    }

    public void setXnStock(Integer xnStock) {
        this.xnStock = xnStock;
    }

    public BigDecimal getJxTax() {
        return jxTax;
    }

    public void setJxTax(BigDecimal jxTax) {
        this.jxTax = jxTax;
    }

    public Integer getProType() {
        return proType;
    }

    public void setProType(Integer proType) {
        this.proType = proType;
    }

    public Integer getInDates() {
        return inDates;
    }

    public void setInDates(Integer inDates) {
        this.inDates = inDates;
    }

    public Date getInStartDate() {
        return inStartDate;
    }

    public void setInStartDate(Date inStartDate) {
        this.inStartDate = inStartDate;
    }

    public Date getInEndDate() {
        return inEndDate;
    }

    public void setInEndDate(Date inEndDate) {
        this.inEndDate = inEndDate;
    }


}