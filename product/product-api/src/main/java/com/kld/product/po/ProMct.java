package com.kld.product.po;

import java.io.Serializable;
import java.util.Date;

public class ProMct implements Serializable{
    private static final long serialVersionUID = 6444600561544177362L;
    /**
     * ID
     */
    private Integer id;

    /**
     * 供应商名称
     */
    private String mctName;

    private String apiCode;

    /**
     * 供应商类型
     */
    private Integer mctType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 组织机构编码
     */
    private String ouCode;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    private Date createTime;

    public String getApiCode() {
        return apiCode;
    }

    public void setApiCode(String apiCode) {
        this.apiCode = apiCode;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMctName() {
        return mctName;
    }

    public void setMctName(String mctName) {
        this.mctName = mctName;
    }

    public Integer getMctType() {
        return mctType;
    }

    public void setMctType(Integer mctType) {
        this.mctType = mctType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOuCode() {
        return ouCode;
    }

    public void setOuCode(String ouCode) {
        this.ouCode = ouCode;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}