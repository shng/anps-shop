package com.kld.product.service.impl;

import com.kld.product.api.IProMctService;
import com.kld.product.dao.ProMctDao;
import com.kld.product.po.ProMct;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
public class ProMctServiceImpl implements IProMctService {

    @Autowired
    private ProMctDao proMctDao;

    @Override
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return proMctDao.delete(id);
    }

    @Override
    public int insert(ProMct proMct) throws Exception {
        return proMctDao.insert(proMct);
    }

    @Override
    public ProMct selectByPrimaryKey(Integer id) throws Exception {
        return proMctDao.get(id);
    }

    @Override
    public int updateByPrimaryKey(ProMct proMct) throws Exception {
        return proMctDao.update(proMct);
    }

    @Override
    public List<ProMct> findProMctList(ProMct proMct) throws Exception {
        return proMctDao.find(proMct);
    }
}
