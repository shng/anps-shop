package com.kld.product.service.impl;

import com.kld.product.api.IProCateBrandRelService;
import com.kld.product.dao.ProCateBrandRelDao;
import com.kld.product.po.ProBrand;
import com.kld.product.po.ProCateBrandRel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
@Service
public class ProCateBrandRelServiceImpl implements IProCateBrandRelService {

    @Autowired
    private ProCateBrandRelDao proCateBrandRelDao;

    @Override
    public int insert(ProCateBrandRel record) throws Exception {
        return proCateBrandRelDao.insert(record);
    }

    @Override
    public int insertList(List<ProCateBrandRel> proCateBrandRelList) throws Exception {
        return proCateBrandRelDao.insert(proCateBrandRelList);
    }

    @Override
    public int updateByPrimaryKey(ProCateBrandRel record) throws Exception {
        return proCateBrandRelDao.update(record);
    }

    @Override
    public List<ProBrand> getBrandByCateId(Integer cateId) throws Exception {
        return proCateBrandRelDao.find("getBrandByCateId",cateId);
    }
}
