package com.kld.product.service.impl;

import com.kld.product.api.IProPrctrueService;
import com.kld.product.dao.ProPrctrueDao;
import com.kld.product.po.ProPrctrue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
@Service
public class ProPrctrueServiceImpl implements IProPrctrueService {

    @Autowired
    private ProPrctrueDao proPrctrueDao;

    @Override
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return proPrctrueDao.delete(id);
    }

    @Override
    public int insert(ProPrctrue proPrctrue) throws Exception {
        return proPrctrueDao.insert(proPrctrue);
    }

    @Override
    public ProPrctrue selectByPrimaryKey(Integer id) throws Exception {
        return proPrctrueDao.get(id);
    }

    @Override
    public int updateByPrimaryKey(ProPrctrue proPrctrue) throws Exception {
        return proPrctrueDao.update(proPrctrue);
    }

    @Override
    public List<ProPrctrue> findProPrctrueList(ProPrctrue proPrctrue) throws Exception {
        return proPrctrueDao.find(proPrctrue);
    }

    @Override
    public List<ProPrctrue> selectByProId(Integer proId) throws Exception {
        return proPrctrueDao.find("selectByProId",proId);
    }
}
