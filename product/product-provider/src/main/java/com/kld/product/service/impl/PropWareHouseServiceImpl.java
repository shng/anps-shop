package com.kld.product.service.impl;

import com.kld.product.api.IPropWareHouseService;
import com.kld.product.dao.PropWareHouseDao;
import com.kld.product.po.PropWareHouse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
@Service
public class PropWareHouseServiceImpl implements IPropWareHouseService {

    @Autowired
    private PropWareHouseDao propWareHouseDao;

    @Override
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return propWareHouseDao.delete(id);
    }

    @Override
    public int insert(PropWareHouse propWareHouse) throws Exception {
        return propWareHouseDao.insert(propWareHouse);
    }

    @Override
    public PropWareHouse selectByPrimaryKey(Integer id) throws Exception {
        return propWareHouseDao.get(id);
    }

    @Override
    public int updateByPrimaryKey(PropWareHouse propWareHouse) throws Exception {
        return propWareHouseDao.update(propWareHouse);
    }

    @Override
    public List<PropWareHouse> findPropWareHouseList(PropWareHouse propWareHouse) throws Exception {
        return propWareHouseDao.find(propWareHouse);
    }
}
