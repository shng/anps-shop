package com.kld.product.service.impl;

import com.kld.product.api.IProServerService;
import com.kld.product.dao.ProServerDao;
import com.kld.product.po.ProServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Created by anpushang on 2016/3/24.
 */
@Service
public class ProServerServiceImpl implements IProServerService {

    @Autowired
    private ProServerDao proServerDao;

    @Override
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return proServerDao.delete(id);
    }

    @Override
    public int insert(ProServer proServer) throws Exception {
        return proServerDao.insert(proServer);
    }

    @Override
    public ProServer selectByPrimaryKey(Integer id) throws Exception {
        return proServerDao.get(id);
    }

    @Override
    public int updateByPrimaryKey(ProServer proServer) throws Exception {
        return proServerDao.update(proServer);
    }

    @Override
    public List<ProServer> findProServerList(ProServer proServer) throws Exception {
        return proServerDao.find(proServer);
    }

    @Override
    public List<ProServer> selectByProId(Integer proId) {
        return proServerDao.find("selectByProId",proId);
    }
}
