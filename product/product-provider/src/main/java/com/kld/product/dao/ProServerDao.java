package com.kld.product.dao;

import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import com.kld.product.po.ProServer;
import org.springframework.stereotype.Repository;

/**
 * Created by anpushang on 2016/3/24.
 */
@Repository
public class ProServerDao extends SimpleDaoImpl<ProServer> {
}
