package com.kld.product.dao;

import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import com.kld.product.po.ProElecTicket;
import org.springframework.stereotype.Repository;

/**
 * Created by anpushang on 2016/3/24.
 */
@Repository
public class ProElecTicketDao extends SimpleDaoImpl<ProElecTicket>{
}
