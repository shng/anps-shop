package com.kld.product.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.common.util.BuildTreeUtil;
import com.kld.product.api.IProCategoryService;
import com.kld.product.dao.ProCateBrandRelDao;
import com.kld.product.dao.ProCategoryDao;
import com.kld.product.dao.ProCatePropRelDao;
import com.kld.product.dao.ProProductDao;
import com.kld.product.dto.CategoryDto;
import com.kld.product.po.ProCateBrandRel;
import com.kld.product.po.ProCatePropRel;
import com.kld.product.po.ProCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utils.CategoryUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by anpushang on 2016/3/24.
 */
@Service
public class ProCategoryServiceImpl implements IProCategoryService {

    @Autowired
    private ProCategoryDao proCategoryDao;
    @Autowired
    private ProCateBrandRelDao proCateBrandRelDao;
    @Autowired
    private ProCatePropRelDao proCatePropRelDao;
    @Autowired
    private ProProductDao proProductDao;

    @Override
    public int deleteByPrimaryKey(Integer id) throws Exception {
        return proCategoryDao.delete(id);
    }

    @Override
    public int insert(ProCategory proCategory) throws Exception {
        return proCategoryDao.insert(proCategory);
    }

    @Override
    public ProCategory selectByPrimaryKey(Integer id) throws Exception {
        return proCategoryDao.get(id);
    }

    @Override
    public int updateByPrimaryKey(ProCategory proCategory) throws Exception {
        return proCategoryDao.update(proCategory);
    }

    @Override
    public List<ProCategory> findCategoryListByLev(ProCategory proCategory) throws Exception {
        return proCategoryDao.find("findCategoryList", proCategory);
    }

    @Override
    public List<ProCategory> findCategoryChild(Integer id) throws Exception {
        return proCategoryDao.find("findCategoryChild",id);
    }

    @Override
    public List<ProCategory> findProCategoryList(ProCategory proCategory) throws Exception {
        return proCategoryDao.find(proCategory);
    }

    @Override
    public PageInfo findProCateGoryPageList(int pageNum, int pageSize, ProCategory proCategory) throws Exception {
        PageHelper.startPage(pageNum, pageSize);
        List<ProCategory> proCategoryList = findProCategoryList(proCategory);
        List<CategoryDto> proCategories = CategoryUtils.buildCategoryTree(proCategoryList, 0);
        PageInfo pageInfo = new PageInfo(proCategories);
        return pageInfo;
    }


    @Override
    public int insertChildCategory(ProCategory proCategory)throws Exception{
        int resultId = proCategoryDao.insert(proCategory); //新增分类返回主键ID

        List<ProCateBrandRel> proCateBrandRelList = new ArrayList<ProCateBrandRel>();//品牌
        List<ProCatePropRel> proCatePropRelList = new ArrayList<ProCatePropRel>();//属性
        List<ProCatePropRel> proSpaceList = new ArrayList<ProCatePropRel>();//规格

        // 插入关系表
        List<Integer> brandList = proCategory.getBrandList();
        if (brandList != null || brandList.size() != 0) {
            for (Integer brandId : brandList) {
                ProCateBrandRel proCateBrandRel = new ProCateBrandRel();
                proCateBrandRel.setCateId(proCategory.getId());
                proCateBrandRel.setBrandId(brandId);
                proCateBrandRelList.add(proCateBrandRel);
            }
        }

        // 插入属性
        List<Integer> propertyList = proCategory.getPropertyList();
        if (null != propertyList|| propertyList.size() != 0) {
            for (Integer propertyId : propertyList) {
                ProCatePropRel catePropRel = new ProCatePropRel();
                catePropRel.setCateId(proCategory.getId());
                catePropRel.setPropId(propertyId);
                proCatePropRelList.add(catePropRel);
            }
        }

        // 插入规格
        List<Integer> specsList = proCategory.getSpecsList();
        if (specsList != null && specsList.size()>0) {
            for (Integer propertyId : specsList) {
                ProCatePropRel catePropRel = new ProCatePropRel();
                catePropRel.setCateId(proCategory.getId());
                catePropRel.setPropId(propertyId);
                proSpaceList.add(catePropRel);
            }
        }

        proCateBrandRelDao.insert(proCateBrandRelList);
        proCatePropRelDao.insert(proCatePropRelList);
        int resultCont = proCatePropRelDao.insert(proSpaceList);
        if (resultCont < 1){
            throw new RuntimeException();
        }
        return resultCont;
    }


    @Override
    public int updateChildCategory(ProCategory proCategory) throws Exception {

        int resultCount = proCategoryDao.update(proCategory); //新增分类返回主键ID
        List<ProCateBrandRel> proCateBrandRelList = new ArrayList<ProCateBrandRel>();//品牌
        List<ProCatePropRel> proCatePropRelList = new ArrayList<ProCatePropRel>();//属性
        List<ProCatePropRel> proSpaceList = new ArrayList<ProCatePropRel>();//规格
        if (resultCount > 0 ){
            //先删除分类与品牌关系数据
            proCateBrandRelDao.delete("deleteCateBrandById", proCategory.getId());
            // 插入品牌表
            List<Integer> brandList = proCategory.getBrandList();
            if (brandList != null || brandList.size() != 0) {
                for (Integer brandId : brandList) {
                    ProCateBrandRel proCateBrandRel = new ProCateBrandRel();
                    proCateBrandRel.setCateId(proCategory.getId());
                    proCateBrandRel.setBrandId(brandId);
                    proCateBrandRelList.add(proCateBrandRel);
                }
            }


//            proCatePropRelDao.delete("deleteCateById",proCategory.getId());
            // 插入属性
            List<Integer> propertyList = proCategory.getPropertyList();
            if (null != propertyList|| propertyList.size() != 0) {
                for (Integer propertyId : propertyList) {
                    ProCatePropRel catePropRel = new ProCatePropRel();
                    catePropRel.setCateId(proCategory.getId());
                    catePropRel.setPropId(propertyId);
                    proCatePropRelList.add(catePropRel);
                }
            }

            if (!propertyList.isEmpty()){
                proCatePropRelDao.insert(proCatePropRelList);
            }

            // 插入规格
            List<Integer> specsList = proCategory.getSpecsList();
            if (specsList != null && specsList.size()>0) {
                for (Integer propertyId : specsList) {
                    ProCatePropRel catePropRel = new ProCatePropRel();
                    catePropRel.setCateId(proCategory.getId());
                    catePropRel.setPropId(propertyId);
                    proSpaceList.add(catePropRel);
                }
            }

            if (!specsList.isEmpty()){
                proCatePropRelDao.insert(proSpaceList);
            }

        }


        int resultCont = proCateBrandRelDao.insert(proCateBrandRelList);
        if (resultCont < 1){
            throw new RuntimeException();
        }
        return resultCont;
    }

    @Override
    public int updateChangePutAway(ProCategory proCategory) throws Exception {

        // 得到分类级别
        Integer lev = proCategory.getLev();
        //得到是否有子分类
        Integer hasChild = proCategory.getHasChild();

        List<Integer> categoryIdList = new ArrayList<Integer>();

        Map<String,Object> objectMap = new ConcurrentHashMap<String,Object>();

        if (lev == 1){ //是一级分类
            categoryIdList.add(proCategory.getId());
            if (hasChild == 1){
                //查询二级分类
                List<ProCategory> categoryChild = findCategoryChild(proCategory.getId());
                if (!categoryChild.isEmpty()){
                    for (ProCategory cateGory : categoryChild) {
                        categoryIdList.add(cateGory.getId());
                        if (cateGory.getHasChild() == 1){ //有子集分类
                            //查询三级分类
                            List<ProCategory> categoryChildList = findCategoryChild(cateGory.getId());
                            for (ProCategory gory : categoryChildList) {
                                categoryIdList.add(gory.getId());
                            }
                        }
                    }
                }else{
                    categoryIdList.add(proCategory.getId());
                }

            }
        }else if (lev == 2){
            categoryIdList.add(proCategory.getId());
            if (hasChild == 1){
                //查询三级分类
                List<ProCategory> categoryChildList = findCategoryChild(proCategory.getId());
                for (ProCategory gory : categoryChildList) {
                    categoryIdList.add(gory.getId());
                }
            }
        }else if (lev == 3){
            //三级分类
            categoryIdList.add(proCategory.getId());
        }
        objectMap.put("state", proCategory.getState());
        objectMap.put("cateList",categoryIdList);
        int changePutAway = proProductDao.update("changePutAway", objectMap);
        return proCategoryDao.update("changePutAway",objectMap);

    }

    @Override
    public int getCateChildById(int id) {
        return proCategoryDao.get("getCateChildById",id);
    }


    @Override
    public List<ProCategory> selectCateByPid(Integer pId) throws Exception {
        return proCategoryDao.find("selectCateByPid",pId);
    }
}
