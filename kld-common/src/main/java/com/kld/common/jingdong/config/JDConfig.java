package com.kld.common.jingdong.config;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * Created by kwg on 2016/3/28.
 */
public class JDConfig {


    public static Configuration config = null;
    /**
     * 配置文件名称
     */
    private static String configfile = "jdConfig.properties";

    static {
        try {
            config = new PropertiesConfiguration(configfile);
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        System.out.println(JDConfig.config.getProperty("proxyHost"));
    }
}
