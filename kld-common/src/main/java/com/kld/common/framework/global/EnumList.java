package com.kld.common.framework.global;

/**
 * Created by 曹不正 on 2016/3/24.
 */
public class EnumList {

    public enum Job {
        Mgr("经理", 1), Staff("员工", 2);
        // 成员变量
        public String text;
        public int value;

        // 构造方法
        private Job(String name, int index) {
            this.text = name;
            this.value = index;
        }

        // 覆盖方法
        @Override
        public String toString() {
            return this.value + "_" + this.text;
        }
    }

    public enum Gendar {
        Male("男", 1), Female("女", 2);
        public int value;
        public String text;

        private Gendar(String text, int value) {
            this.value = value;
            this.text = text;
        }
    }

    public enum Channel {
        Web("WEB", 0), Weixin("微信", 1), App("APP", 2);
        public int value;
        public String text;

        private Channel(String text, int value) {
            this.value = value;
            this.text = text;
        }
    }

    /**
     * 积分兑换订单状态
     */
    public static enum OrderState {
        UnPaid("未支付",1),Paying("支付中",2),Paied("已支付",3),Delivering("待收货",4),Deliverd("已妥投",5),DENIED("已拒收",6),Abandoned("已废弃",7),CANCELED("已取消",8),Finished("已完成",9);
        public int value;
        public String text;
        private OrderState(String text, int value) {
            this.value = value;
            this.text = text;
        }

        public static String getText(int value){
            String stateText = "";
            switch (value){
                case  1: stateText=OrderState.UnPaid.text; break;
                case  2: stateText=OrderState.Paying.text; break;
                case  3: stateText=OrderState.Paied.text; break;
                case  4: stateText=OrderState.Delivering.text; break;
                case  5: stateText=OrderState.Deliverd.text; break;
                case  6: stateText=OrderState.DENIED.text; break;
                case  7: stateText=OrderState.Abandoned.text; break;
                case  8: stateText=OrderState.CANCELED.text; break;
                case  9: stateText=OrderState.Finished.text; break;
                default: stateText = ""; break;
            }
            return stateText;
        }
    }


    /**
     * 营销活动类型
     */
    public static enum ActType {
        Monthy("月抽奖",1),ExpandPionts("积分抽奖",2),GoldenEgg("砸金蛋",3),Promotion("打折促销",4);
        public int value;
        public String text;
        private ActType(String text, int value) {
            this.value = value;
            this.text = text;
        }
    }

    /**
     * 活动奖品类型
     */
    public static enum PrizeType {
        Physical("实体",1),Virtual("虚拟",2);
        public int value;
        public String text;
        private PrizeType(String text, int value) {
            this.value = value;
            this.text = text;
        }
    }


    /**
     * 营销活动状态
     */
    public static enum ActState {
        UnPublish("未发布",1),Launching("已发布",2),Expired("已结束",3),PrePublist("预发布",4);
        public int value;
        public String text;
        private ActState(String text, int value) {
            this.value = value;
            this.text = text;

        }

        public static String getText(int state){
            String stateStr = "";
            switch (state){
                case 1: stateStr=UnPublish.text; break;
                case 2:stateStr=Launching.text; break;
                case 3:stateStr=Expired.text; break;
                case 4:stateStr=PrePublist.text; break;
                default:stateStr="";break;
            }
            return stateStr;
        }
    }

    /**
     * 月抽奖活动开奖状态
     */
    public static enum MonthActOpenState {
        UnOpened("未开奖",1),Openning("抽奖中",2),Opened("已抽奖",3),Published("已发布",4);
        public int value;
        public String text;
        private MonthActOpenState(String text, int value) {
            this.value = value;
            this.text = text;
        }
    }

    /**
     * 月抽奖活动开奖状态   -1-未领奖，1-待发奖，2-已发奖
     */
    public static enum ActOrderState {
        Unrecived("未领奖",-1),Openning("待发奖",1),Delivered("已发奖",2);
        public int value;
        public String text;
        private ActOrderState(String text, int value) {
            this.value = value;
            this.text = text;
        }
    }

    public static enum ExceptionCase{
        ShopNonResp("积分商城未响应",1),DiffRes("卡系统对账文件与交易查询结果不一致",2),DiffPoints("交易积分额不一致",3),
        DiffCardNo("油卡号不一致",4),CardNotExistOrder("卡系统对账文件未包含该订单",5),
        DiffMctAmounts("合作商金额与积分商城不同",6),MctOrderFail("合作商下单失败",7),Others("其它异常",8),
        ErrorShopState("积分商城状态错误",9),ErrorCardState("卡核心状态错误",10);
        public int value;
        public String text;
        private ExceptionCase(String text,int value){
            this.value=value;
            this.text=text;
        }
    }

    public static enum ImgClsId{
        WebTitleimg("WEB特推专区主题图",1),
        WebBackgroundimg("WEB特推专区背景图",2),
        WebItemBannerimg("WEB落地页顶部图",5),
        WebItemBackgroundimg("WEB落地页列表主题图",6),
        WebItemBackgroundcolor("WEB落地页全局默认背景色",7),
        WebItemListBackgroundcolor("WEB落地页列表默认背景色",11),
        WxTitleimg("微信/APP特推专区主题图",3),
        WxBackgroundimg("微信/APP特推专区背景图",4),
        WxItemBannerimg("微信/APP落地页顶部图",8),
        WxItemBackgroundimg("微信/APP落地页列表主题图",9),
        WxItemBackgroundcolor("微信/APP落地页全局默认背景色",10),
        WxItemListBackgroundcolor("微信/APP落地页列表默认背景色",12);
        public int value;
        public String text;
        private ImgClsId(String text, int value) {
            this.value = value;
            this.text = text;
        }
    }

}
