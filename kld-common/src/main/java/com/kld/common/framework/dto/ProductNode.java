package com.kld.common.framework.dto;

import java.io.Serializable;

/**
 * Created by anpushang on 2016/4/5.
 */
public class ProductNode implements Serializable {
    private static final long serialVersionUID = -1439083093729836602L;

    private String id;
    private String text;
    private String pid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
}
