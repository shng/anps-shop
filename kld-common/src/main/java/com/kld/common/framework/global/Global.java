package com.kld.common.framework.global;

/**
 * Created by 曹不正 on 2016/3/24.
 */
public class Global {
    //菜单顶级父ID
    public static String FUN_TOP_PID="-1";
    public static String CATE_TOP_PID="0";
    public static String ORG_TOP_PID="0";

    //操作类型
    public static Integer OPERATION_TYPE_DEFAULT=0;
    public static Integer OPERATION_TYPE_ADD=1;  //添加
    public static Integer OPERATION_TYPE_DELETE=2; //删除
    public static Integer OPERATION_TYPE_UPDATE=3; //修改
    public static Integer OPERATION_TYPE_CHECK=4;
    public static Integer OPERATION_TYPE_PASS=5;  //审核通过
    public static Integer OPERATION_TYPE_UNPASS=6; //审核拒绝

    //定时任务，合作商标记
    public static final String TASK_MERCHANT_NAME="1000";
    //京东消息 项目正式上线时 请修改 delType 为 1
    public static final Integer JD_MESSAGE_DELTYPE=0;
    //积分价格转换 倍率
//    public static final Integer baseNumber=1000;
    //对账失败，重新尝试次数和每次的间隔时间
    public static final Integer checkNum=3;  //次数
    public static final Integer checkTime=1 * 60 * 1000; //时间，单位:毫秒

    //------定义缓存变量

    //商品类别
    public static final String INDEX_CATEGORY="category";


    //region 积分商城Web首页缓存key
    //热兑品类
    public static final String INDEX_HOTCATEGORY="hotcategory";
    //车队兑换专区
    public static final String INDEX_HOTPRODUCT="hotproduct";
    //积分换大奖区
    public static final String INDEX_JIFENPRIZE="jifenprize";
    //最新动态
    public static final String INDEX_ARTICLE="article";
    //热门商品
    public static final String INDEX_PROFORVEHICLE="proForVehicle";
    //广告展示
    public static final String INDEX_AD="ad";
    //endregion

    //region 积分商城微信首页缓存key

    //热兑品类
    public static final String WXAPP_INDEX_HOTCATEGORY="wxapp_hotcategory";
    //车队兑换专区
    public static final String WXAPP_INDEX_HOTPRODUCT="wxapp_hotproduct";
    //积分换大奖区
    public static final String WXAPP_INDEX_JIFENPRIZE="wxapp_jifenprize";
    //最新动态
    public static final String WXAPP_INDEX_ARTICLE="wxapp_article";
    //热门商品
    public static final String WXAPP_INDEX_PROFORVEHICLE="wxapp_proForVehicle";
    //广告展示
    public static final String WXAPP_INDEX_AD="wxapp_ad";
    //endregion

    //region 京东接口地址缓存
    //京东省
    public static final String JD_PROVICE="province";
    //京东市
    public static final String JD_CITY="city";
    //京东区县
    public static final String JD_COUNTY="county";
    //京东镇
    public static final String JD_TOWN="town";
    //endregion

    //列表页热门商品
    public static final String PROLIST_HOTPRODUCT="prolist_hotproduct";

    //一小时
    public static final Integer REDIS_ONEHOUR = 60*60*60;
    //一天
    public static final Integer REDIS_ONEDAY = 60*60*60*24;
    //半天
    public static final Integer REDIS_HALFDAY = 60*60*60*12;
    //一个月
    public static final Integer REDIS_ONEMONTH = 60*60*60*24*30;

    public static final boolean IsPaySkipModel = false;

    public static final String FRONT_SESSION_USERNAME="cusername";
    public static final String FRONT_SESSION_USERID="cuserid";
    public static final String FRONT_SESSION_WXUSERNAME="wxusername";
    public static final String FRONT_SESSION_WXUSERID="wxuserid";

    //APP端图片限制（控制流量）
    public static final Integer APP_PIMGS_MAXNUM = 5;

}
