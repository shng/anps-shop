package com.kld.common.util;

/**
 * 生成图片等文件的名字
 * @author tufei
 *
 */
public class CreateFileName {
	public static String getFileName(){
		return java.util.UUID.randomUUID().toString();
	}
	public static void main(String[] args) {
		System.out.println(java.util.UUID.randomUUID().toString());
	}
}
