package com.kld.common.util;

import org.apache.oro.text.regex.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by Dan on 2016/1/5.
 */
public class RegExpUtil {
    /**
     *
     * 手机号 规则说明：
     中国大陆：开头1 3-8号段，后边跟9位数字
     台湾：09开头后面跟8位数字
     香港：9或6开头后面跟7位数字
     澳门：66或68开头后面跟5位数字
     */

    //*Regexp匹配模式
    public static String PostCodeRegexp = "[0-9]\\d{5}(?!\\d)";  //邮政编码的匹配模式
//    public static String PhoneRegexp = "([0-9]{4})+-([0-9]{6,8})+";//固话的匹配模式
    public static String MobileRegexp = "^[1][3-8]\\d{9}$|^([6|9])\\d{7}$|^[0][9]\\d{8}$|^[6]([8|6])\\d{5}$"; //手机的匹配模式

    private static PatternCompiler compiler = new Perl5Compiler();
    private static PatternMatcher matcher = new Perl5Matcher();
    private static Logger logger = LoggerFactory.getLogger(RegExpUtil.class);


    /**
     * 校验邮政编码是否合法
     */
    public static boolean isPostCode(String postCode){
        Pattern pattern = null;
        try {
            pattern = compiler.compile(PostCodeRegexp) ;
            return   matcher.matches(postCode,pattern);
        } catch (MalformedPatternException e) {
            logger.error(e.getMessage());
        }
        return false;
    }
   /* *//**
     * 校验固定电话是否合法
     *//*
    public static boolean isPhone(String phone){
        Pattern pattern = null;
        try {
            pattern = compiler.compile(PhoneRegexp) ;
            return   matcher.matches(phone,pattern);
        } catch (MalformedPatternException e) {
            logger.error(e.getMessage());
        }
        return false;
    }*/
    /**
     * 校验手机号码是否合法
     */
    public static boolean isMobile(String mobile){
        Pattern pattern = null;
        try {
            pattern = compiler.compile(MobileRegexp) ;
            return   matcher.matches(mobile,pattern);
        } catch (MalformedPatternException e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(isMobile("6811111"));
//        System.out.println(isPhone("0912-888888"));
        System.out.println(isPostCode("100001"));
    }
}
