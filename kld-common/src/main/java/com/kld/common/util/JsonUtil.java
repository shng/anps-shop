package com.kld.common.util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.Iterator;

/**
 * Created by jw on 2015/9/17.
 */
public class JsonUtil {

    public static JSONArray listtoJSONArray(Object object){
        JSONArray jsonArray = JSONArray.fromObject(object);
        return jsonArray;
    }

    public static JSONArray formatAddrToJsonArray(String jsonStr){
        JSONArray jsonArray = new JSONArray();
        if(jsonStr.startsWith("{") && jsonStr.endsWith("}")) {
            JSONObject jsonObject = JSONObject.fromObject(jsonStr);
            if (jsonObject.toString().length() > 0) {
                try {
                    for (Iterator iter = jsonObject.keys(); iter.hasNext(); ) {
                        String key = iter.next().toString();
                        JSONObject json = new JSONObject();
                        json.put("id", jsonObject.get(key));
                        json.put("name", key);
                        jsonArray.add(json);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return jsonArray;
    }

}
