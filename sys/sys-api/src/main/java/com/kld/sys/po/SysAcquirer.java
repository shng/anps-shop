package com.kld.sys.po;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by xiaohe on 2016/3/28.
 */
public class SysAcquirer implements Serializable {
    private static final long serialVersionUID = -6073990150846425213L;
    private Integer id;
    private String acceptCode;
    private String ouCode;
    private String ouCodeName;
    private String name;
    private String secret;
    private String acount;
    private Integer isUsed;
    private String creator;
    private Date updateTime;
    public Integer getId() {
        return id;
    }


    public String getOuCode() {
        return ouCode;
    }

    public String getName() {
        return name;
    }

    public String getSecret() {
        return secret;
    }

    public String getAcount() {
        return acount;
    }

    public Integer getIsUsed() {
        return isUsed;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setOuCode(String ouCode) {
        this.ouCode = ouCode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public void setAcount(String acount) {
        this.acount = acount;
    }

    public void setIsUsed(Integer isUsed) {
        this.isUsed = isUsed;
    }

    public String getCreator() {
        return creator;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getOuCodeName() {
        return ouCodeName;
    }

    public void setOuCodeName(String ouCodeName) {
        this.ouCodeName = ouCodeName;
    }

    public String getAcceptCode() {
        return acceptCode;
    }

    public void setAcceptCode(String acceptCode) {
        this.acceptCode = acceptCode;
    }
}
