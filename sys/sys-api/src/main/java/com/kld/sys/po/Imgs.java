package com.kld.sys.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by 曹不正 on 2016/3/28.
 */
public class Imgs implements Serializable {
    private static final long serialVersionUID = -189111137367120687L;
    private BigDecimal id;

    private BigDecimal bsnsid;

    private BigDecimal clsid;

    private String picurl;

    private BigDecimal isdel;

    private String oucode;

    private String creator;

    private Date createtime;

    private Date modifytime;

    private BigDecimal isprimary;

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getBsnsid() {
        return bsnsid;
    }

    public void setBsnsid(BigDecimal bsnsid) {
        this.bsnsid = bsnsid;
    }

    public BigDecimal getClsid() {
        return clsid;
    }

    public void setClsid(BigDecimal clsid) {
        this.clsid = clsid;
    }

    public String getPicurl() {
        return picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl == null ? null : picurl.trim();
    }

    public BigDecimal getIsdel() {
        return isdel;
    }

    public void setIsdel(BigDecimal isdel) {
        this.isdel = isdel;
    }

    public String getOucode() {
        return oucode;
    }

    public void setOucode(String oucode) {
        this.oucode = oucode == null ? null : oucode.trim();
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getModifytime() {
        return modifytime;
    }

    public void setModifytime(Date modifytime) {
        this.modifytime = modifytime;
    }

    public BigDecimal getIsprimary() {
        return isprimary;
    }

    public void setIsprimary(BigDecimal isprimary) {
        this.isprimary = isprimary;
    }

}
