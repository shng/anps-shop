package com.kld.sys.api;

import com.kld.sys.po.SysUser;

import java.util.HashMap;
import java.util.List;

/**
 * Created by 曹不正 on 2016/3/29.
 */
public interface IUserDetailsService{
    List<HashMap<String,Object>> getauthorityPointListByUserId(String UserID);
}
