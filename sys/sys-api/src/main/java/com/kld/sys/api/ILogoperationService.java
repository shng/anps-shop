package com.kld.sys.api;

import com.kld.sys.po.*;

import java.util.Map;

/**
 *
 */
public interface ILogoperationService {
    int insert(Logoperation record);

    Logoperation createLogperation(Map<String, String> logmap);


}
