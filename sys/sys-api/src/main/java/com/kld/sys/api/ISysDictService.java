package com.kld.sys.api;

import com.kld.sys.po.SysDict;

import java.util.List;
import java.util.Map;
/**
 * hejinping 2016.3.24
 */
public interface ISysDictService {

    int insert(SysDict sys_dict);
    int update(SysDict sys_dict);
    int updateValidState(int dictcode, int valid);
    int delete(int dictid);
    List<SysDict> getSysDictList(Map<String, Object> map);
    SysDict getSysDict(int dictcode);
    SysDict getSysDictByAlias(String alias);
    List<SysDict> getSysDictListByPcode(int parentcode);
}
