package com.kld.sys.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Logoperation implements Serializable{

    private static final long serialVersionUID = -5671083053178535213L;
    private BigDecimal id;

    private BigDecimal operationtype;

    private String content;

    private String createuser;

    private Date createtime;

    private String operationobject;

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getOperationtype() {
        return operationtype;
    }

    public void setOperationtype(BigDecimal operationtype) {
        this.operationtype = operationtype;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getCreateuser() {
        return createuser;
    }

    public void setCreateuser(String createuser) {
        this.createuser = createuser == null ? null : createuser.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getOperationobject() {
        return operationobject;
    }

    public void setOperationobject(String operationobject) {
        this.operationobject = operationobject == null ? null : operationobject.trim();
    }
}