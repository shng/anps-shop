package com.kld.sys.api;

import com.kld.sys.po.Imgs;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by 曹不正 on 2016/3/24.
 */
public interface ImgsService {

    int insertSelective(Imgs record);

    Imgs selectByPrimaryKey(BigDecimal id);

    int updateByPrimaryKeySelective(Imgs record);

    List<Imgs> getImgListByActid(Integer actid);

    int updatePrimaryByBsnsid(BigDecimal bsnsid,boolean isprimary);

    Imgs getPrimaryImg(BigDecimal bsnsid, int clsid);

}
