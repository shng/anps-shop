package com.kld.sys.service.impl;
import com.kld.common.framework.dto.TreeNode;
import com.kld.sys.api.ISysOrgUnitService;
import com.kld.sys.dao.SysOrgUnitDao;
import com.kld.sys.po.SysOrgUnit;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
/**
 * hejinping 2016.3.23
 */
@Service
public class SysOrgUnitServiceImpl implements ISysOrgUnitService {

    @Resource
    private SysOrgUnitDao sys_orgunitDao;

    @Override
    public int updateByPrimaryKeySelective(SysOrgUnit record) {
        return sys_orgunitDao.update("updateByPrimaryKeySelective",record);
    }

    @Override
    public List<SysOrgUnit> getSysOrgunitList(Map<String, Object> map) {
        return sys_orgunitDao.find("getSysOrgunitList",map);
    }

    @Override
    public SysOrgUnit getSysOrgunitByOuCode(String oucode) {
        return sys_orgunitDao.get("selectByPrimaryKey",oucode);
    }
    @Override
    public List<TreeNode> getTreeNodesByPCode(String pcode){
        return sys_orgunitDao.find("getTreeNodesByPCode",pcode);
    }
}
