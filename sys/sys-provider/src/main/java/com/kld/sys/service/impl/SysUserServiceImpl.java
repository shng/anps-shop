package com.kld.sys.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.framework.page.PageInfoResult;
import com.kld.sys.api.ISysApprovalService;
import com.kld.sys.api.ISysUserService;
import com.kld.sys.dao.SysApprovalDao;
import com.kld.sys.dao.SysUserDao;
import com.kld.sys.po.SysApproval;
import com.kld.sys.po.SysRole;
import com.kld.sys.po.SysUser;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * hejinping 2016.3.23
 */
@Service
public class SysUserServiceImpl implements ISysUserService {


  @Autowired
  private SysUserDao sys_userDao;
  @Autowired
  private SysApprovalDao sysApprovalDao;
//  @Autowired
//  private ISysApprovalService sysApprovalService;
  @Override
  public ResultMsg findSysUserByUserName(String userName) {
    ResultMsg ResultMsg=new ResultMsg();
    List<SysUser> orgUsers= null;
    sys_userDao.delete("deleteByPrimaryKey","dad");
            //userMapperDao.queryOrgUserByUserID(userID);
    if(orgUsers.size()>0){
      ResultMsg.setResult(true);
      ResultMsg.setData(orgUsers);
    }else{
      ResultMsg.setResult(false);
    }
    return ResultMsg;
  }

  @Override
  public SysUser querySysUserByUserName(String userName) {
    return sys_userDao.get("selectByPrimaryKey",userName);
  }

  @Override
  public SysUser selectUserMoreInfo(String username){
    return sys_userDao.get("selectUserMoreInfo",username);
  }

  @Override
  public ResultMsg<SysUser> getSysUserList(Map<String,Object> map,Integer pageNum,Integer pageSize){
    ResultMsg rmsg = new ResultMsg();
    PageHelper.startPage(pageNum, pageSize);
    List<SysUser> list = sys_userDao.find("getSysUserList", map);

    // 分页
    PageInfo pageInfo = new PageInfo(list);

    return PageInfoResult.PageInfoMsg(pageInfo);
  }

  @Override
  public int insert(SysUser user){
      return sys_userDao.insert(user);
  }

  @Transactional(readOnly = false, propagation = Propagation.REQUIRED,rollbackFor = {Exception.class})
  @Override
  public ResultMsg insertWithTrans(SysUser user) throws Exception{
    ResultMsg res = new ResultMsg();
    int r = insert(user);
    if (r > 0) {
      SysApproval sysApproval = new SysApproval();
      sysApproval.setBsnsid(user.getUsername());
      sysApproval.setApprovetype(2);
      sysApprovalDao.insert(sysApproval);
      res.setResult(true);
      res.setMsg("添加成功！");
      res.setData(user.getUsername());

    } else {
      res.setResult(false);
      res.setMsg("创建账户失败");
    }
    return res;
  }

  @Override
  public int update(SysUser user) {
    return sys_userDao.update("updateByPrimaryKey",user);
  }
  @Override
  public List<SysUser> getSysUserListByRolecode(String roleCode){
    return sys_userDao.find("getSysUserListByRolecode",roleCode);

  }

  @Override
  public ResultMsg getApprovalUserList(int pageNum,int pageSize) {
    PageHelper.startPage(pageNum, pageSize, false);
    PageInfo pageInfo=new PageInfo(sys_userDao.find("getApprovalUserList",null));
    return  PageInfoResult.PageInfoMsg(pageInfo);
  }

  @Override
  public int updateByPrimaryKeySelective(SysUser sys_user) {
    return sys_userDao.update("updateByPrimaryKeySelective",sys_user);
  }

  @Override
  public SysUser getUserByUserName(String username) {
    return sys_userDao.get("getUserByUserName",username);
  }

  @Override
  public int updateAppBatch(SysUser sysUser, HashMap<String, Object> map) {
    sys_userDao.update("updateByPrimaryKeySelective",sysUser);
    return sysApprovalDao.update("updateApprovalUser",map);

  }
}