package com.kld.sys.service.impl;

import com.kld.sys.api.ISysDictService;
import com.kld.sys.dao.SysDictDao;
import com.kld.sys.po.SysDict;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * hejinping 2016.3.24
 */
@Service
public class SysDictServiceImpl implements ISysDictService {

    @Resource
    private SysDictDao sys_dictDao;

    @Override
    public int insert(SysDict sys_dict) {
        return sys_dictDao.insert(sys_dict);
    }

    @Override
    public int update(SysDict sys_dict) {
        return sys_dictDao.update(sys_dict);
    }

    @Override
    public int updateValidState(int dictcode, int isvalid) {
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("dictcode",dictcode);
        map.put("isvalid",isvalid);
        return sys_dictDao.update("updateValidState",map);
    }


    @Override
    public int delete(int dictid) {
        return sys_dictDao.delete(dictid);
    }

    @Override
    public List<SysDict> getSysDictList(Map<String, Object> map) {
        return sys_dictDao.find("getSysDictList",map);
    }

    @Override
    public SysDict getSysDict(int dictcode) {
        return sys_dictDao.get("getSysDict",dictcode);

    }

    @Override
    public SysDict getSysDictByAlias(String alias) {
        return sys_dictDao.get("getSysDictByAlias",alias);
    }

    @Override
    public List<SysDict> getSysDictListByPcode(int parentcode){
        return sys_dictDao.find("getSysDictListByPcode",parentcode);
    }

}
