package com.kld.sys.service.impl;

import com.kld.sys.api.ISysFuncService;
import com.kld.sys.dao.SysFuncDao;
import com.kld.sys.po.SysFunc;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * hejinping 2016.3.24
 */
@Service
public class SysFuncServiceImpl implements ISysFuncService {
    @Resource
    private SysFuncDao sys_funcDao;

    @Override
    public List<SysFunc> getFuncList(Map<String,Object> map){
        return sys_funcDao.find("getFuncList",map);
    }

}
