package com.kld.sys.dao;

import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import com.kld.sys.po.SysFunc;
import org.springframework.stereotype.Repository;

/**
 * hejinping 2016.3.23
 */
@Repository
public class SysFuncDao extends SimpleDaoImpl<SysFunc> {

}