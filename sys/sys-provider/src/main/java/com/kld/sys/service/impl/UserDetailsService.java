/**
 * 58.com Inc.
 * Copyright (c) 2005-2015 All Rights Reserved.
 */
package com.kld.sys.service.impl;

import com.kld.sys.api.IUserDetailsService;
import com.kld.sys.dao.UserAuthorityPointDao;
import com.kld.sys.po.UserAuthorityPoint;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsService implements IUserDetailsService{

    @Resource
    private UserAuthorityPointDao userAuthorityPointDao;

    @Override
    public List<HashMap<String,Object>> getauthorityPointListByUserId(String UserID) {
        return userAuthorityPointDao.find("getauthorityPointListByUserId",UserID);
    }

}
