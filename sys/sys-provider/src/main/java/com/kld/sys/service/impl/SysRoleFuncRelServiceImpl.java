package com.kld.sys.service.impl;


import com.kld.sys.api.ISysRoleFuncRelService;
import com.kld.sys.dao.SysRoleFuncRelDao;
import com.kld.sys.po.SysRoleFuncRel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * hejinping 2016.3.24
 */
@Service
public class SysRoleFuncRelServiceImpl implements ISysRoleFuncRelService {
    @Autowired
    private SysRoleFuncRelDao sysRoleFuncRelDao;

    @Override
    public int deleteRoleFuncByRolecode(String rolecode){
        return sysRoleFuncRelDao.delete("deleteRoleFuncByRolecode",rolecode);
    }
    public int insert(SysRoleFuncRel sysRoleFuncRel){
        return sysRoleFuncRelDao.insert(sysRoleFuncRel);
    }

}
