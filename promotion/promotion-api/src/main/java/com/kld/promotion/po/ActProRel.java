package com.kld.promotion.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ActProRel implements Serializable {
    private static final long serialVersionUID = 8923422092164906066L;
    private BigDecimal id;

    private BigDecimal actid;

    private BigDecimal productid;

    private BigDecimal discountrate;   //活动优惠率

    private BigDecimal points;     //活动优惠价

    private Date createtime;
    private String creator;

    private BigDecimal isdel;

    private Date modifytime;

    private String modifier;

    private BigDecimal num;
    private BigDecimal salenum;
    private BigDecimal weight;   //权重
    private BigDecimal mctoriginprice; //京东原价*1.07（）
    private BigDecimal mctoriginpoints;//京东原价*1.07（积分）（仅前台展示用）


    private String productname;  //商品名称
    private BigDecimal actprice;   //活动价格
    private BigDecimal pmctoriginprice ;   //协议价
    private BigDecimal mctprice;   //协议价
    private BigDecimal suggestpoints;   //建议积分
    private BigDecimal originpoints;   //兑换积分
    private BigDecimal selfprice;   //运营价格
    private String thirdcode;   //第三方商品编码

    private BigDecimal originselfprice;  //原运营价格
    private String productcode;  //商品编码
    private String mctcode;   //供应商编码
    private String mctname;  //供应商名称

    private String acttitle;  //活动主题
    private BigDecimal profit;  //毛利
    private BigDecimal totalprofit;  //总毛利
    private BigDecimal totalmctprice;  //总成本
    private BigDecimal discountpoints;  //单品折扣（分）
    private BigDecimal discount;  //单品折扣（元）
    private BigDecimal tradepoints; //交易总积分
    private BigDecimal tradeamount; //交易总金额
    private BigDecimal tradediscountpoints;  //已交易商品折扣（分）
    private BigDecimal tradediscount;  //已交易商品折扣（元）
    private BigDecimal deliverednum;  //妥投数量
    private BigDecimal delivereddiscountpoints;  //已妥投商品折扣（分）
    private BigDecimal delivereddiscount;  //已妥投商品折扣（元）

    public BigDecimal getTotalprofit() {
        return totalprofit==null?BigDecimal.valueOf(0):totalprofit;
    }

    public void setTotalprofit(BigDecimal totalprofit) {
        this.totalprofit = totalprofit;
    }

    public BigDecimal getTotalmctprice() {
        return totalmctprice==null?BigDecimal.valueOf(0):totalmctprice;
    }

    public void setTotalmctprice(BigDecimal totalmctprice) {
        this.totalmctprice = totalmctprice;
    }

    public BigDecimal getMctoriginprice() {
        return mctoriginprice;
    }

    public void setMctoriginprice(BigDecimal mctoriginprice) {
        this.mctoriginprice = mctoriginprice;
    }

    public BigDecimal getMctoriginpoints() {
        return mctoriginpoints;
    }

    public void setMctoriginpoints(BigDecimal mctoriginpoints) {
        this.mctoriginpoints = mctoriginpoints;
    }

    public BigDecimal getTradepoints() {
        return tradepoints==null?BigDecimal.valueOf(0):tradepoints;
    }

    public void setTradepoints(BigDecimal tradepoints) {
        this.tradepoints = tradepoints;
    }

    public BigDecimal getTradeamount() {
        return tradeamount==null?BigDecimal.valueOf(0):tradeamount;
    }

    public void setTradeamount(BigDecimal tradeamount) {
        this.tradeamount = tradeamount;
    }

    public BigDecimal getOriginselfprice() {
        return originselfprice;
    }

    public void setOriginselfprice(BigDecimal originselfprice) {
        this.originselfprice = originselfprice;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public BigDecimal getDiscountpoints() {
        return discountpoints;
    }

    public void setDiscountpoints(BigDecimal discountpoints) {
        this.discountpoints = discountpoints;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getTradediscountpoints() {
        return tradediscountpoints==null?BigDecimal.valueOf(0):tradediscountpoints;
    }

    public void setTradediscountpoints(BigDecimal tradediscountpoints) {
        this.tradediscountpoints = tradediscountpoints;
    }

    public BigDecimal getTradediscount() {
        return tradediscount==null?BigDecimal.valueOf(0):tradediscount;
    }

    public void setTradediscount(BigDecimal tradediscount) {
        this.tradediscount = tradediscount;
    }

    public BigDecimal getDeliverednum() {
        return deliverednum==null?BigDecimal.valueOf(0):deliverednum;
    }

    public void setDeliverednum(BigDecimal deliverednum) {
        this.deliverednum = deliverednum;
    }

    public BigDecimal getDelivereddiscountpoints() {
        return delivereddiscountpoints==null?BigDecimal.valueOf(0):delivereddiscountpoints;
    }

    public void setDelivereddiscountpoints(BigDecimal delivereddiscountpoints) {
        this.delivereddiscountpoints = delivereddiscountpoints;
    }

    public BigDecimal getDelivereddiscount() {
        return delivereddiscount==null?BigDecimal.valueOf(0):delivereddiscount;
    }

    public void setDelivereddiscount(BigDecimal delivereddiscount) {
        this.delivereddiscount = delivereddiscount;
    }

    public BigDecimal getSuggestpoints() {
        return suggestpoints;
    }

    public void setSuggestpoints(BigDecimal suggestpoints) {
        this.suggestpoints = suggestpoints;
    }

    public BigDecimal getOriginpoints() {
        return originpoints;
    }

    public void setOriginpoints(BigDecimal originpoints) {
        this.originpoints = originpoints;
    }

    public BigDecimal getSelfprice() {
        return selfprice;
    }

    public void setSelfprice(BigDecimal selfprice) {
        this.selfprice = selfprice;
    }

    public String getThirdcode() {
        return thirdcode;
    }

    public void setThirdcode(String thirdcode) {
        this.thirdcode = thirdcode;
    }

    public BigDecimal getPmctoriginprice() {
        return pmctoriginprice;
    }

    public void setPmctoriginprice(BigDecimal pmctoriginprice) {
        this.pmctoriginprice = pmctoriginprice;
    }


  public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public BigDecimal getActprice() {
        return actprice;
    }

    public void setActprice(BigDecimal actprice) {
        this.actprice = actprice;
    }

    public BigDecimal getMctprice() {
        return mctprice;
    }

    public void setMctprice(BigDecimal mctprice) {
        this.mctprice = mctprice;
    }

    public String getProductcode() {
        return productcode;
    }

    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    public String getMctcode() {
        return mctcode;
    }

    public void setMctcode(String mctcode) {
        this.mctcode = mctcode;
    }

    public String getMctname() {
        return mctname;
    }

    public void setMctname(String mctname) {
        this.mctname = mctname;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getActid() {
        return actid;
    }

    public void setActid(BigDecimal actid) {
        this.actid = actid;
    }

    public BigDecimal getProductid() {
        return productid;
    }

    public void setProductid(BigDecimal productid) {
        this.productid = productid;
    }

    public BigDecimal getDiscountrate() {
        return discountrate;
    }

    public void setDiscountrate(BigDecimal discountrate) {
        this.discountrate = discountrate;
    }

    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public BigDecimal getIsdel() {
        return isdel;
    }

    public void setIsdel(BigDecimal isdel) {
        this.isdel = isdel;
    }

    public Date getModifytime() {
        return modifytime;
    }

    public void setModifytime(Date modifytime) {
        this.modifytime = modifytime;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public BigDecimal getSalenum() {
        return salenum ==null?BigDecimal.valueOf(0):salenum;
    }

    public void setSalenum(BigDecimal salenum) {
        this.salenum = salenum;
    }
    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public String getActtitle() {
        return acttitle;
    }

    public void setActtitle(String acttitle) {
        this.acttitle = acttitle;
    }
}