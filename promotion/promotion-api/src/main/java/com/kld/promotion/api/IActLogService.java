package com.kld.promotion.api;

import com.kld.bi.po.Report;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.promotion.po.Actlog;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 曹不正 on 2016/3/24.
 */
public interface IActLogService {
    int deleteByPrimaryKey(BigDecimal id);

    int insert(Actlog record);

    int insertSelective(Actlog record);

    Actlog selectByPrimaryKey(BigDecimal id);

    int updateByPrimaryKeySelective(Actlog record);

    int updateByPrimaryKey(Actlog record);

    List<Actlog> getActlogList(Map<String,Object> map);

    List<Actlog> getActlogs(Map<String, Object> map);

    List<Actlog> getActlogListByActid(Map<String,Object> map);

    List<Actlog> getActlogListbyUserid(Map<String,Object> map);

    List<String> getMonthWinnerIds(Integer actid);

    /**
     * 查询所有参加小积分抽大奖活动的用户消耗積分
     */
    BigDecimal getJFSumPoints(BigDecimal actid,Integer minActLogId);

    Actlog getTop1ActTimesByUserID(String userID);

    public ResultMsg insertByRegister(String userid, String username);


    /**
     * 查询所有参加小积分抽大奖活动中已中奖，产生的成本
     * @return
     */
    Double getJFCost(BigDecimal actid);

    List<Actlog> getPointsActlogListBill(HashMap<String, Object> map);

    Integer updatePayState(BigDecimal id, int value);

    Integer updateCheckCard(BigDecimal id, String billDate);

//    Integer updatePayState(List<Integer> ids, String paystate);

    Integer updatePayStates(List<Integer> ids, String paystate);

    Integer getLastWinLogId(BigDecimal actId);

    int updatepCardPaiedTime(Date cardPaiedTime, Integer id);

    List<Integer> getIdList();

    Map<String,Object> getPointsAndNum(String billdate);

    List<Report> getActlogDataForReport(String billdate);

}
