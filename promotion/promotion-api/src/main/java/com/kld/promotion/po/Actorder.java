package com.kld.promotion.po;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by 曹不正 on 2016/3/24.
 */
public class Actorder implements Serializable {
    private static final long serialVersionUID = -1060847908919787483L;
    private BigDecimal id;
    private BigDecimal actlogid; //抽奖日志id
    private String userid;

    private BigDecimal actitemid;

    private BigDecimal actid;

    private String realname;

    private BigDecimal provinceid;

    private BigDecimal cityid;

    private BigDecimal countyid;

    private BigDecimal townid;

    private String address;

    private String phone;

    private BigDecimal points;

    private BigDecimal acttype;

    private String deliveryno;

    private Date deliverytime;

    private String express;

    private String oucode;

    private BigDecimal state;   //-1为领奖

    private String zip;

    private String remark;

    private String handler;

    private Date createtime;
    private String strcreatetime;
    private String cardtradeno;

    private Date cardpaiedtime;



    private BigDecimal iswon;//是否中奖

    private String username;//用户名

    private String title;//活动主题

    private String cardno; //支付积分卡号

    private String belongareaid; //支付油卡的归属地ID
    private String belongareaname; //支付油卡的归属地名称

    private BigDecimal prizeid;   //奖品ID

    private String prizename;  //奖品名称

    private BigDecimal actitemtype;  //奖项类型

    private String acttitle; //活动主题

    private BigDecimal prizetype; //奖品类型

    private String prizeimg; //奖品图片

    private String areaname;//区域信息

    private double selfprice;//奖品单价

    private String provincename; //省名称
    private String cityname; //市名称
    private String countyname; //县、区名称
    private String townname; //镇名称

    public String getProvincename() {
        return provincename;
    }

    public String getCityname() {
        return cityname;
    }

    public String getCountyname() {
        return countyname;
    }

    public String getTownname() {
        return townname;
    }

    public double getSelfprice() {
        return selfprice;
    }

    public void setSelfprice(double selfprice) {
        this.selfprice = selfprice;
    }

    public String getStrcreatetime() {
        return strcreatetime;
    }

    public void setStrcreatetime(String strcreatetime) {
        this.strcreatetime = strcreatetime;
    }

    public String getAreaname() {
        areaname="";
        areaname+= StringUtils.isNotBlank(provincename)?(provincename+" "):"";
        areaname+=StringUtils.isNotBlank(cityname)?(cityname+" "):"";
        areaname+=StringUtils.isNotBlank(countyname)?(countyname+" "):"";
        areaname+=StringUtils.isNotBlank(townname)?townname:"";
        return areaname;
    }

//    public void setAreaname(String areaname) {
//        this.areaname = areaname;
//    }

    public BigDecimal getPrizeid() {
        return prizeid;
    }

    public void setPrizeid(BigDecimal prizeid) {
        this.prizeid = prizeid;
    }

    public BigDecimal getActitemtype() {
        return actitemtype;
    }

    public void setActitemtype(BigDecimal actitemtype) {
        this.actitemtype = actitemtype;
    }

    public String getActtitle() {
        return acttitle;
    }

    public void setActtitle(String acttitle) {
        this.acttitle = acttitle;
    }

    public BigDecimal getPrizetype() {
        return prizetype;
    }

    public void setPrizetype(BigDecimal prizetype) {
        this.prizetype = prizetype;
    }

    public String getPrizeimg() {
        return prizeimg;
    }

    public void setPrizeimg(String prizeimg) {
        this.prizeimg = prizeimg;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public BigDecimal getIswon() {
        return iswon;
    }

    public void setIswon(BigDecimal iswon) {
        this.iswon = iswon;
    }

    public String getPrizename() {
        return prizename = prizename == null ? "" : prizename;
    }

    public void setPrizename(String prizename) {
        this.prizename = prizename;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getZip() {
        return zip = zip == null ? "" : zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid == null ? null : userid.trim();
    }

    public BigDecimal getActitemid() {
        return actitemid;
    }

    public void setActitemid(BigDecimal actitemid) {
        this.actitemid = actitemid;
    }

    public BigDecimal getActid() {
        return actid;
    }

    public void setActid(BigDecimal actid) {
        this.actid = actid;
    }

    public String getRealname() {
        return realname = realname == null ? "" :realname;
    }

    public void setRealname(String realname) {
        this.realname = realname == null ? null : realname.trim();
    }

    public BigDecimal getProvinceid() {
        return provinceid==null?BigDecimal.valueOf(0):provinceid;
    }

    public void setProvinceid(BigDecimal provinceid) {
        this.provinceid = provinceid;
    }

    public BigDecimal getCityid() {
        return cityid==null?BigDecimal.valueOf(0):cityid;
    }

    public void setCityid(BigDecimal cityid) {
        this.cityid = cityid;
    }

    public BigDecimal getCountyid() {
        return countyid==null?BigDecimal.valueOf(0):countyid;
    }

    public void setCountyid(BigDecimal countyid) {
        this.countyid = countyid;
    }

    public BigDecimal getTownid() {
        return townid==null?BigDecimal.valueOf(0):townid;
    }

    public void setTownid(BigDecimal townid) {
        this.townid = townid;
    }

    public String getAddress() {
        return address=address == null ? "" :address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getPhone() {
        return phone = phone == null ? "" : phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public BigDecimal getPoints() {
        return points = points ;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }

    public BigDecimal getActtype() {
        return acttype;
    }

    public void setActtype(BigDecimal acttype) {
        this.acttype = acttype;
    }

    public String getDeliveryno() {
        return deliveryno = deliveryno == null ? "" : deliveryno;
    }

    public void setDeliveryno(String deliveryno) {
        this.deliveryno = deliveryno == null ? null : deliveryno.trim();
    }

    public Date getDeliverytime() {
        return deliverytime;
    }

    public void setDeliverytime(Date deliverytime) {
        this.deliverytime = deliverytime;
    }

    public String getExpress() {
        return express = express == null ? "" : express;
    }

    public void setExpress(String express) {
        this.express = express == null ? null : express.trim();
    }

    public String getOucode() {
        return oucode;
    }

    public void setOucode(String oucode) {
        this.oucode = oucode == null ? null : oucode.trim();
    }

    public BigDecimal getState() {
        return state;
    }

    public void setState(BigDecimal state) {
        this.state = state;
    }

    public String getHandler() {
        return handler;
    }

    public void setHandler(String handler) {
        this.handler = handler;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getCardtradeno() {
        return cardtradeno;
    }

    public void setCardtradeno(String cardtradeno) {
        this.cardtradeno = cardtradeno;
    }

    public Date getCardpaiedtime() {
        return cardpaiedtime;
    }

    public void setCardpaiedtime(Date cardpaiedtime) {
        this.cardpaiedtime = cardpaiedtime;
    }

    public String getBelongareaid() {
        return belongareaid;
    }

    public void setBelongareaid(String belongareaid) {
        this.belongareaid = belongareaid;
    }

    public String getBelongareaname() {
        return belongareaname;
    }

    public void setBelongareaname(String belongareaname) {
        this.belongareaname = belongareaname;
    }

    public BigDecimal getActlogid() {
        return actlogid;
    }

    public void setActlogid(BigDecimal actlogid) {
        this.actlogid = actlogid;
    }

}
