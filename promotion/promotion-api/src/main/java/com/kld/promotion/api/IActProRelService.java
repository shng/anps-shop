package com.kld.promotion.api;

import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.product.po.Product;
import com.kld.promotion.po.ActProRel;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by 曹不正 on 2016/3/24.
 */
public interface IActProRelService {
    int insertSelective(ActProRel record);

    ActProRel selectByPrimaryKey(BigDecimal id);

    int updateByPrimaryKeySelective(ActProRel record);

    List<ActProRel> getActprorelByActid(Integer actid);

    List<ActProRel> getActProList(Map<String, Object> map);
    PageInfo getActProList(Map<String, Object> map,int PageNum,int PageSize);
    List<Product> getProList(Map<String, Object> map);

    ResultMsg insertWithTrans(BigDecimal actid, List<Integer> proids, String username) throws Exception;

    ResultMsg deleteLogic(String ids);

    ResultMsg setActProRule(String idsStr, BigDecimal discountrate, BigDecimal points, BigDecimal num) throws Exception;

    //查询当前活动与已发布促销活动重复的商品信息
    List<ActProRel> pickRepeatActProducts(Integer actid);

    ResultMsg setActProWeight(Integer id, BigDecimal weight);

    ResultMsg cancelActProWeight(List<Integer> ids);

    List<ActProRel> getActProTradeList(Map<String, Object> map);

}
