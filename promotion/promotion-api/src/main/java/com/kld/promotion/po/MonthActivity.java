package com.kld.promotion.po;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by 曹不正 on 2016/3/28.
 */
public class MonthActivity extends Activity implements Serializable {
    private static final long serialVersionUID = 8068420433006168001L;
    private Integer totaluser;  //参与总人数

    private String month; //月抽奖的月份

    private Date opentime; // 开奖时间

    private String prizecontent; //奖品内容

    private Date publishtime; //发布时间

    public Integer getTotaluser() {
        return totaluser;
    }

    public void setTotaluser(Integer totaluser) {
        this.totaluser = totaluser;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Date getOpentime() {
        return opentime;
    }

    public void setOpentime(Date opentime) {
        this.opentime = opentime;
    }

    public String getPrizecontent() {
        prizecontent="";
        List<Actitem> items = getActitems();
        if(items!=null && items.size()>0) {
            for (Iterator<Actitem> iterator = items.iterator(); iterator.hasNext(); ) {
                Actitem item = iterator.next();
                prizecontent += item.getPrizename() + ", ";
            }
        }
        return prizecontent.length()>0? prizecontent.substring(prizecontent.length()-2):"";
    }

    public Date getPublishtime() {
        return publishtime;
    }

    public void setPublishtime(Date publishtime) {
        this.publishtime = publishtime;
    }


}
