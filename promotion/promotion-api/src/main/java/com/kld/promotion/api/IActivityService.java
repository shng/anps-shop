package com.kld.promotion.api;

import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.framework.global.EnumList;
import com.kld.promotion.po.Activity;
import com.kld.promotion.po.Prize;
import com.kld.promotion.po.PrizeInfo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by 曹不正 on 2016/3/24.
 * 很多功能没有实现
 */
public interface IActivityService {
    int deleteByPrimaryKey(BigDecimal id);

    int insert(Activity record);

    int insertSelective(Activity record);

    Activity selectByPrimaryKey(BigDecimal id);

    int updateByPrimaryKeySelective(Activity record);

    int updateByPrimaryKey(Activity record);

    List<Activity> getActList(Map<String, Object> map);

    PageInfo getActList(Map<String, Object> map, int PageNum, int PageSize);
    /**
     * 逻辑删除
     * @param id
     * @return
     */
    int deleteLogicByID(Integer id);

    List<Prize> getLimitJFActList(Integer num);

    List<Prize> getJFActList(Map<String,Object> map);


    List<Activity> getMonthActList(Map<String, Object> stringObjectHashMap);

    ResultMsg startAct(String idsStr) throws Exception;

    Integer editFilter(Integer actid, Integer minpoints, Integer mincount, Integer filterlogic);

    List<Activity> getActListByType(Integer type);

    List<Activity> getActListByType_1(Integer type, Date currTime, Integer flag);

    List<Activity> getActListByPid(BigDecimal pid,Date startdate,Date enddate);

    List<PrizeInfo> queryPrizesByUserId(String userid, Integer num, Integer state, Integer acttype);

    List<Activity> getActListByEndtime(Date currentTime);

    Integer queryActTimesByUserID(String userID);

    ResultMsg smashingEggs(String userID, String userName, Integer channel) throws Exception;

    boolean checkTitleIfUsed(String title);

    ResultMsg publishAct(Integer actid) throws Exception;

    ResultMsg publishMonthWinner(Integer actid) throws Exception;

    ResultMsg taskGenerateMonthAct(Activity pact) throws Exception;

    BigDecimal getPointsById(BigDecimal id);

    ResultMsg endAct(Integer actid) throws Exception;

    ResultMsg payPointsAct(BigDecimal actID, String userID, String userName, String cardNo, EnumList.Channel channel) throws Exception;

    //获取砸金蛋中奖后的领奖地址
    String getAddress(String alias);

    List <Activity>getActListByState(Integer state);
    Integer updateStateToLaunching (Integer state,Date currDate,Integer id);
    Integer finishActivity(Integer state,Date currentTime,Integer id);

    List<Activity> getIndexActProList(Integer limit);

}
