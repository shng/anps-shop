package com.kld.promotion.dao;

import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import com.kld.promotion.po.Activity;
import com.kld.promotion.po.PrizeInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 曹不正 on 2016/3/24.
 */
@Repository
public class ActivityDao extends SimpleDaoImpl<Activity> {
    public List<Activity> getActListByType_1(@Param("type") Integer type, @Param("currTime") Date currTime, @Param("flag") Integer flag){
        HashMap map = new HashMap();
        map.put("type",type);
        map.put("currTime",currTime);
        map.put("flag",flag);
        return find("getActListByType_1",map);
    }
    public List<Activity> getActListByPid(@Param("pid")BigDecimal pid, @Param("startdate") Date startdate, @Param("enddate")Date enddate){
        HashMap map = new HashMap();
        map.put("pid",pid);
        map.put("startdate",startdate);
        map.put("enddate",enddate);
        return find("getActListByPid",map);
    }
    public List<PrizeInfo> queryPrizesByUserId(@Param("userid")String userid, @Param("num")Integer num, @Param("state")Integer state, @Param("acttype")Integer acttype){
        HashMap map = new HashMap();
        map.put("userid",userid);
        map.put("num",num);
        map.put("state",state);
        map.put("acttype",acttype);
        return find("queryPrizesByUserId",map);
    }
    public Integer updateStateToLaunching (@Param("state") Integer state,@Param("currDate") Date currDate,@Param("id") Integer id){
        HashMap map = new HashMap();
        map.put("state",state);
        map.put("currDate",currDate);
        map.put("id",id);
        return get("updateStateToLaunching",map);
    }
    public Integer finishActivity(@Param("state") Integer state,@Param("currentTime") Date currentTime,@Param("id") Integer id){
        HashMap map = new HashMap();
        map.put("state",currentTime);
        map.put("currDate",id);
        return get("finishActivity",map);
    }

}
