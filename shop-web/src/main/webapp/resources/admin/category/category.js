
(function () {
    $('#pId').combotree({
        url: '/admin/cate/getCategory',
        method: 'get',
        onSelect:selectCate

    });

    //上传控件
    var state = 'pending',
        uploader = WebUploader.create({
            swf: '/../../resource/js/Uploader.swf',
            // 不压缩image
            resize: false,
            // 文件接收服务端。
            server: '/admin/cate/uploadPic',
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            accept: {title: 'Images', extensions: 'gif,jpg,jpeg,bmp,png', mimeTypes: 'image/*'},
            pick: {id: '#picker', multiple: false, innerHTML: '点击选择图片'},
            auto: true //设置自动上传文件
        });

    uploader.on('fileQueued', function (file) {
        uploader.makeThumb(file, function (error, src) {
            if (error) {
                return;
            }
            $("#thelist").empty().append('<img src="' + src + '">');
        }, 90, 90);

    });
    //捕获错误提示
    uploader.on('error', function (reason) {
        if (reason == 'Q_TYPE_DENIED') {
            $("#msg_upload").empty().html("<font color='red'>文件类型错误</font>");
        }
    })
    // 成功前会派送一个事件uploadAccept，这个事件是用来询问是否上传成功的
    uploader.on('uploadAccept', function (file, response) {
        if (!response.result) {
            // 通过return false来告诉组件，此文件上传有错。
            $("#msg_upload").empty().html("文件上传出错");
            return false;
        }
        else {
            $("#msg_upload").empty().html("文件上传成功!");
            $("#picImg").val(response.msg);
            return true;
        }
    });

})();

function imgFormatter(value, row, index) {
    if ('' != value && null != value){
        return value = '<img style="width:30px; height:30px" src="' + value + '">';
    }else{
        return "";
    }
}

var recommend=[{"value":0,"text":"不显示"},{"value":1,"text":"显示"}];
var formatRecommend=function(value){
    for(var key in recommend){
        if(recommend[key].value == value){
            return recommend[key].text;
        }
    }
    return "-";
}


var categroyState=[{"value":0,"text":"下架"},{"value":1,"text":"上架"}];
var formatCategroyState=function(value){
    for(var key in categroyState){
        if(categroyState[key].value == value){
            return categroyState[key].text;
        }
    }
    return "-";
}

$("#btnAdd").on("click", function () {
    window.location.href = "/admin/cate/toAddCatePage";
});


function selectCate(row) {
    var level = row.level; //得到级别
    $("#lev").val(level+1);

    if(level == 2){
        $("#LinkBrandId").show();
        $("#linkPropertyId").show();
        $("#linkSpecId").show();
        $("#hasChild").val(0);
    }else{
        $("#LinkBrandId").hide();
        $("#linkPropertyId").hide();
        $("#linkSpecId").hide();
        $("#hasChild").val(1);
    }

}


function btnSaveAd(){

    var brand_value = [];//定义一个数组
    $('input[name="brandArray"]:checked').each(function () {//遍历每一个名字为interest的复选框，其中选中的执行函数
        brand_value.push($(this).val());//将选中的值添加到数组chk_value中
    });

    var property_value = [];//定义一个数组
    $('input[name="propertyArray"]:checked').each(function () {//遍历每一个名字为interest的复选框，其中选中的执行函数
        property_value.push($(this).val());//将选中的值添加到数组chk_value中
    });

    var spec_value = [];//定义一个数组
    $('input[name="specArray"]:checked').each(function () {//遍历每一个名字为interest的复选框，其中选中的执行函数
        spec_value.push($(this).val());//将选中的值添加到数组chk_value中
    });

    $.ajax({
        type: "post",
        url: "/admin/cate/saveCategory",
        data: $('#fmInsertCateForm').serialize(),
        dataType: 'json',
        success: function (data) {
            if(data.result){
                $.messager.alert("提示",data.msg);
                window.location.href = "/admin/cate/toList";
            }else{
                $.messager.alert("提示",data.errorMsg);
            }
        }
    });
}

/***
 * 编辑
 */
$("#btnEdit").on("click",function(){
    var row = $("#categoryList").datagrid('getSelected');
    if(row == null){
        $.messager.alert("提示","请选择操作行");
        return;
    }
    var id = row.id;//属性ID

    window.location.href = "/admin/cate/toUpdateCatePage?id="+id;
});


/***
 * 删除
 */
$("#btnRemove").on("click",function(){
    var row = $("#categoryList").datagrid('getSelected');
    if(row == null){
        $.messager.alert("提示","请选择操作行");
        return;
    }
    var id = row.id;//属性ID
    var lev = row.lev;
    $.ajax({
        type: "post",
        url: "/admin/cate/getSub/"+id+"/"+lev,
        dataType: 'json',
        success: function (data) {
            if(data.result){ //判断分类下是否有子集分类，或者是三级分类是否被商品使用
                $.messager.alert("提示",data.msg);
                return false;
            }else{
                $.ajax({
                    type: "post",
                    url: "/admin/cate/deleteCate?id="+id,
                    dataType: 'json',
                    success: function (data) {
                        if(data.result){
                            $.messager.alert("提示1",data.msg);
                            $('#categoryList').treegrid('reload');
                        }else{
                            $.messager.alert("提示",data.errorMsg);
                        }
                    }
                });
            }
        }
    });
});


$("#changePutAway").on("click",function(){
    var row = $("#categoryList").datagrid('getSelected');
    if(row == null){
        $.messager.alert("提示","请选择操作行");
        return;
    }
    var id = row.id;//属性ID
    var lev = row.lev;
    var hasChild = row.hasChild;
    var state = row.cateState;
    var cateSta = state == 1 ? 0 : 1 ;
    $.ajax({
        type: "post",
        url: "/admin/cate/changePutAway",
        data: {"id": id,"lev":lev,"hasChild":hasChild,"state":cateSta},
        dataType: 'json',
        success: function (data) {
            if(data.result){
                $.messager.alert("提示1",data.msg);
                $('#categoryList').treegrid('reload');
            }else{
                $.messager.alert("提示",data.errorMsg);
            }
        }
    });
});

/***
 * 查看
 */
$("#lookDetail").on("click",function(){
    var row = $("#categoryList").datagrid('getSelected');
    if(row == null){
        $.messager.alert("提示","请选择操作行");
        return;
    }
    var id = row.id;//属性ID
    window.location.href = "/admin/cate/lookDetail?id="+id;
});

/*function initIsshowField(level) {
    if (typeof(level) != "undefined" && level == 1) {
        $("#txt_recommend").combobox({disabled: false, required: true});
    } else {
        $("#txt_recommend").combobox({disabled: true, required: false});
    }
}*/



