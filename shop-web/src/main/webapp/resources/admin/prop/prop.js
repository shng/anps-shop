/***
 * 是否规格格式化
 * @type {{value: number, text: string}[]}
 */
var propSpec = [{"value": 0, "text": "否"}, {"value": 1, "text": "是"}];
var formatPropSpec = function (value) {
    for (var key in propSpec) {
        if (propSpec[key].value == value) {
            return propSpec[key].text;
        }
    }
    return "-";
}


/***
 * 数据状态格式化
 * @type {{value: number, text: string}[]}
 */
var propState = [{"value": 0, "text": "禁用"}, {"value": 1, "text": "启用"}];
var formatPropState = function (value) {
    for (var key in propState) {
        if (propState[key].value == value) {
            return propState[key].text;
        }
    }
    return "-";
}


/***
 * 属性类型格式化
 * @type {{value: number, text: string}[]}
 */
var propType = [{"value": 0, "text": "文本框"}, {"value": 1, "text": "单选"}, {"value": 2, "text": "多选"}];
var formatPropType = function (value) {
    for (var key in propType) {
        if (propType[key].value == value) {
            return propType[key].text;
        }
    }
    return "-";
}


$("#btnAdd").on("click", function () {
    window.location.href = "/admin/prop/toAddPropPage";
});

var $specificationTable;
/**
 * 添加属性值行
 */
(function () {



    $("#addSpect").on("click", function () {

        var html = '<tr class="specificationValueTr"><td><input type="text" name="propertyValues" value="" class="dele_inp" placeholder="请输入属性值"/></td>' +
            '<td><input type="text" name="sorts"  class="dele_number" placeholder="0"/></td>' +
            '<td><a href="#" onclick="deleteSpec(this)">删除</a></td></tr>';

        $("#spectValues").append(html);

        $specificationTable = $("#spectValues");
    });

    $("input:radio[name='propType']").on("change", changetype);
    $("input[name='propName']").on("blur", checkName);

    $("input:radio[name='isSpec']").bind("change", function () {
        var that = this;
        if (that.value == 1) {
            $('#checkbox').attr("checked", true);
            $("#need").attr("checked", true);
            $('input:radio[name="propType"]').attr("disabled", true);
            $('input:radio[name="isNeed"]').attr("disabled", true);
        } else {
            $('input:radio[name="propType"]').attr("disabled", false);
            $('input:radio[name="isNeed"]').attr("disabled", false);
        }
    });
})();


function checkName() {
    var name = $("input[name='propName']").val();
    var flag;
    $.ajax({
        type: "post",
        url: "/admin/prop/checkPropName",
        data: "propName=" + name,
        dataType: 'json',
        success: function (data) {
            if (data.result) {
            }else{
                $.messager.alert("提示", data.msg);
            }
            flag = data.result;
        }
    });
    return flag;
}


function check() {
    var isSubmit = true;
    checkName();
    var status = 1;
    $("#spectValues").find("input[name='propertyValues']").each(function () {
        var specVal = $(this).val();
        if (specVal.length < 1 || specVal.length > 20) {
            status = 2;
        }
        ;
    });

    $("#spectValues").find("input[name='sorts']").each(function () {
        var sort = $(this).val();
        if (!/^[0-9]{0,10}$/.test(sort)) {
            status = 3;
        }
        ;
    });
    var type = $('input:radio[name="propType"]:checked').val();
    var isSpec = $('input:radio[name="isSpec"]:checked').val();
    if (type != 0 || isSpec == 1) {
        if (status == 2) {
            isSubmit = false;
            $("#spectValues").next(".promsg").text("属性值只能1-20位").show();
        } else if (status == 3) {
            isSubmit = false;
            $("#spectValues").next(".promsg").text("属性值序号只能为数字").show();
        } else {
            $("#spectValues").next(".promsg").hide();
        }
    }

    if ($(".promsg:visible").length > 0) {
        isSubmit = false;
    }
    return isSubmit;

}


function changetype(event) {
    var src = event.target || event.srcelement;
    var val = src.value;
    if (val == 2) {
        $('input:radio[name="isSpec"]').attr("disabled", false);
        $(".toolPropList").css("display", "inline");
    } else if (val == 1) {
        $('input:radio[name="isSpec"]').attr("disabled", true);
        $(".toolPropList").css("display", "inline");
    } else {
        $('input:radio[name="isSpec"]').attr("disabled", true);
        $(".toolPropList").css("display", "none");
    }
}


// 删除商品规格值
function deleteSpec(obj) {
    var $this = $(obj);
    if ($specificationTable.find(".specificationValueTr").length <= 1) {
        $.messager.alert("提示", "必须至少保留一个属性值!");
    } else {
        $this.parent().parent().remove();
    }
}
/**
 * 新增
 */
function btnSaveAd(){
    var isSubmit = check();
    if (isSubmit == false) {
        $.messager.alert("提示", "属性属性值信息不完整或不符合规范，请修改!");
        return false;
    }else{
        $.ajax({
            type: "post",
            url: "/admin/prop/saveProp",
            data: $('#fmInsertPropForm').serialize(),
            dataType: 'json',
            success: function (data) {
                if(data.result){
                    $.messager.alert("提示","新增成功");
                    window.location.href = "/admin/prop/toList";
                }else{
                    $.messager.alert("提示","操作失败");
                }
            }
        });
    }
}

function btnUpdateProp(){
    var isSubmit = check();
    if (isSubmit == false) {
        $.messager.alert("提示", "属性属性值信息不完整或不符合规范，请修改!");
        return false;
    }else{
        $.ajax({
            type: "post",
            url: "/admin/prop/upPropById",
            data: $('#fmUpdatePropForm').serialize(),
            dataType: 'json',
            success: function (data) {
                if(data.result){
                    $.messager.alert("提示","新增成功");
                    window.location.href = "/admin/prop/toList";
                }else{
                    $.messager.alert("提示",data.errorMsg);
                }
            }
        });
    }
}

/***
 * 编辑
 */
$("#btnEdit").on("click",function(){
    var row = $("#gridPropList").datagrid('getSelected');
    if(row == null){
        $.messager.alert("提示","请选择操作行");
        return;
    }
    var propId = row.id;//属性ID

    window.location.href = "/admin/prop/getBrandById?id="+propId;
});



/**
 * 删除
 */
$("#btnRemove").on("click", function () {
    var row = $("#gridPropList").datagrid('getSelected');
    if(row == null){
        $.messager.alert("提示","请选择操作行");
        return;
    }
    var propId = row.id;//属性ID
    $(function () {
        $.messager.confirm("操作提示", "您确定要执行操作吗？", function (data) {
            if (data) {
                $.ajax({
                    type: "post",
                    cache: false,
                    url: "/admin/prop/verPassed",
                    data: "id=" + propId,
                    async: true,
                    dataType: 'json',
                    success: function (data) {
                        if (!data.result) {
                            $.ajax({
                                type: "post",
                                cache: false,
                                url: "/admin/prop/delPropById",
                                data: "id=" + propId,
                                async: true,
                                dataType: 'json',
                                success: function (data) {
                                    if (data.result) {
                                        $.messager.alert("提示", data.msg);
                                        $("#gridPropList").datagrid('reload');
                                    }
                                },error:function(){
                                    $.messager.alert("提示","操作失败,请稍后尝试");
                                }
                            });
                        } else {
                            $.messager.alert("提示","你选择的属性已绑定商品不能删除");
                        }
                    }
                });
            }
        });
    });
});




