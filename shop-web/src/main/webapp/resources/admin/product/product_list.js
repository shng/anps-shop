$("#btnAdd").on("click",function(){
    window.location.href = "/admin/pro/toAddProduct";
});

$("#btnEdit").on("click",function(){
    var row = $("#gridProductList").datagrid('getSelected');
    if(row == null){
        $.messager.alert("提示","请选择操作行");
        return;
    }
    var id = row.id;//属性ID
    window.location.href = "/admin/pro/toUpdatePage?id="+id;
});


$("#btnProductSetting").on("click",function(){
    var row = $("#gridProductList").datagrid('getSelected');
    if(row == null){
        $.messager.alert("提示","请选择操作行");
        return;
    }
    var id = row.id;//属性ID
    window.location.href = "/admin/pro/toProdcutSettingPage?id="+id;
});