
(function(){
    //上传控件
    var state = 'pending',
        uploader = WebUploader.create({
            swf: '/../../resource/js/Uploader.swf',
            // 不压缩image
            resize: false,
            // 文件接收服务端。
            server: '/admin/ad/uploadImg',
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            accept: { title: 'Images',extensions: 'gif,jpg,jpeg,bmp,png',mimeTypes: 'image/*' },
            pick: { id: '#picker', multiple: false ,innerHTML:'点击选择图片'},
            auto:true //设置自动上传文件
        });

    uploader.on('fileQueued', function (file) {
        uploader.makeThumb(file, function (error, src) {
            if (error) {
                return;
            }
            $("#thelist").empty().append('<img id="img" src="' + src + '">');
        }, 150, 90);

    });
    //捕获错误提示
    uploader.on('error', function(reason){
        if(reason=='Q_TYPE_DENIED'){
            $("#msg_upload").empty().html("<font color='red'>文件类型错误</font>");
        }
    })
    // 成功前会派送一个事件uploadAccept，这个事件是用来询问是否上传成功的
    uploader.on('uploadAccept', function (file, response) {
        if (!response.result) {
            // 通过return false来告诉组件，此文件上传有错。
            $("#msg_upload").empty().html("文件上传出错");
            return false;
        }
        else {
            $("#msg_upload").empty().html("文件上传成功!");
            $("#txt_imgpath").val(response.msg);
            return true;
        }
    });

})();


function imgFormatter(value, row, index) {
    if ('' != value && null != value){
        return value = '<img style="width:30px; height:30px" src="' + value + '">';
    }else{
        return "";
    }
}



var brandState=[{"value":0,"text":"否"},{"value":1,"text":"是"}];
var formatBrandState=function(value){
    for(var key in brandState){
        if(brandState[key].value == value){
            return brandState[key].text;
        }
    }
    return "-";
}

//得到数据表格
var gridBrandList=$("#gridBrandList");
//得到添加页面
var openDialogInsertBrand = $("#openDialogInsertBrand");
//得到修改页面

//获得数据表格form
var fmInsertBrandForm = $("#fmInsertBrandForm");
//保存url
var saveBrandURL = "/admin/brand/saveBrand";
//修改url
var updateBrandURL = "/admin/brand/upBrandById";

/***
 * 新增品牌
 */
$("#btnAdd").on("click", function () {
    openDialogInsertBrand.dialog('setTitle','添加品牌');
    fmInsertBrandForm.form("reset");
    openDialogInsertBrand.dialog("open").dialog("vcenter");
});

function btnSaveAd(){
    fmInsertBrandForm.form("submit",{
        url:saveBrandURL,
        onSubmit: function(){
            return $(this).form("validate");
        },
        success:function(data){
            var res = $.parseJSON(data);
            if(res.result){
                $.messager.alert("提示",res.msg);
                openDialogInsertBrand.dialog("close");
                gridBrandList.datagrid('reload');
            }else{
                $.messager.alert("提示",res.msg);
            }
        }
    });
}

/**
 * 修改品牌
 */
$("#btnEdit").on("click", function () {
    openDialogInsertBrand.dialog('setTitle','修改品牌');
    fmInsertBrandForm.form("reset");
    var row = gridBrandList.datagrid('getSelected');
    if(row == null){
        $.messager.alert("提示","请选择操作行");
        return;
    }
    var logo = row.logo;
    var imageHtml = "<img src='"+logo+"' width='90px;' height='80px;'>";
    $("#thelist").html(imageHtml)
    fmInsertBrandForm.form("load",row);
    openDialogInsertBrand.dialog("open").dialog("vcenter");
});


$("#btnRemove").on("click", function () {
    var row = gridBrandList.datagrid('getSelected');
    var brandId = row.id;
    $.ajax({
        url: '/admin/brand/delBrandById',
        type: 'post',
        dataType:'json',
        data: {"id": brandId},
        success: function (data) {
            if (data.result) {
                $.messager.alert("提示", data.msg);
                gridBrandList.datagrid('reload');
            }
            else {
                $.messager.alert("提示", data.msg);
            }

        }
    });
});


