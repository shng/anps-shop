/**
 * Created by Dan on 2015/12/2.
 */

function addToCart(proid,proname, num,points,picurl,callback){
    var totalNum = new Number($$("#txtTotalQuantity").text());
    var cart = new Object();
    cart.PROID=new Number(proid);
    cart.QUANTITY = new Number(num);
    cart.POINTS = new Number(points);
    cart.PRODUCTNAME = proname;
    cart.PRODUCTURL =picurl;
    mui.ajax("/api/cart/addprotocart",{
        data:cart,
        dataType:"json",
        type:"POST",
        success:function(data){
            if(data.result){
                if(typeof (callback)=="function"){
                    callback();
                }else {
                    mui.toast('加入购物车成功');
                    $$("#txtTotalQuantity").text(totalNum+ new Number(num));
                }
            }else{
                mui.toast('加入购物车失败');
            }
        }
    });
}

function toCreateOrder(proid,num){
    var proinfo = proid+":"+num;
    window.location.href="/api/order/order?productsinfo="+proinfo;
}