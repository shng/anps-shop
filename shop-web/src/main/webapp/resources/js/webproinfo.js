$(document).ready(function(){
    var interval = $("#imageMenu li:first").width();
    var totalcount = $("#imageMenu li").length;
    //var curcount = parseInt($("#imageMenu").width/interval);
    // 图片上下滚动
    var count = totalcount - 5; /* 显示 6 个 li标签内容 */
    //var count = $("#imageMenu li").length - 5; /* 显示 6 个 li标签内容 */

    var curIndex = 0;
    if(count<=0) {$('.smallImgDown').addClass('disabled');}
    $('.scrollbutton').click(function(){
        if( $(this).hasClass('disabled') ) return false;

        if ($(this).hasClass('smallImgUp')) --curIndex;
        else ++curIndex;

        $('.scrollbutton').removeClass('disabled');
        if (curIndex == 0) $('.smallImgUp').addClass('disabled');
        if (curIndex >= count-1) $('.smallImgDown').addClass('disabled');
        $("#imageMenu ul").stop(false, true).animate({"marginLeft" : -curIndex*interval + "px"}, 600);
    });
    var midChangeHandler = null;
    $("#imageMenu li img").bind("click", function(){
        if ($(this).attr("id") != "onlickImg") {
            midChange($(this).attr("src").replace("small", "mid"));
            $("#imageMenu li").removeAttr("id");
            $(this).parent().attr("id", "onlickImg");
        }
    }).bind("mouseover", function(){
        if ($(this).attr("id") != "onlickImg") {
            window.clearTimeout(midChangeHandler);
            midChange($(this).attr("src").replace("small", "mid"));
            $(this).css({ "border": "1px solid #A52A2A" });
        }
    }).bind("mouseout", function(){
        if($(this).attr("id") != "onlickImg"){
            $(this).removeAttr("style");
            midChangeHandler = window.setTimeout(function(){
                midChange($("#onlickImg img").attr("src").replace("small", "mid"));
            }, 1000);
        }
    });
    $("#imageMenu li:first img").click();
    function midChange(src) {
        $("#midimg").attr("src", src).load(function() {
            changeViewImg();
        });
    }
    //大视窗看图
    function mouseover(e) {
        if ($("#winSelector").css("display") == "none") {
            $("#winSelector,#bigView").show();
        }
        $("#winSelector").css(fixedPosition(e));
        e.stopPropagation();
    }
    function mouseOut(e) {
        if ($("#winSelector").css("display") != "none") {
            $("#winSelector,#bigView").hide();
        }
        e.stopPropagation();
    }
    $("#midimg").mouseover(mouseover); //中图事件
    $("#midimg,#winSelector").mousemove(mouseover).mouseout(mouseOut); //选择器事件

    var $divWidth = $("#winSelector").width(); //选择器宽度
    var $divHeight = $("#winSelector").height(); //选择器高度
    var $imgWidth = $("#midimg").width(); //中图宽度
    var $imgHeight = $("#midimg").height(); //中图高度
    var $viewImgWidth = $viewImgHeight = $height = null; //IE加载后才能得到 大图宽度 大图高度 大图视窗高度

    function changeViewImg() {
        $("#bigView img").attr("src", $("#midimg").attr("src").replace("mid", "big"));
    }
    changeViewImg();
    $("#bigView").scrollLeft(0).scrollTop(0);
    function fixedPosition(e) {
        if (e == null) {
            return;
        }
        var $imgLeft = $("#midimg").offset().left; //中图左边距
        var $imgTop = $("#midimg").offset().top; //中图上边距
        X = e.pageX - $imgLeft - $divWidth / 2; //selector顶点坐标 X
        Y = e.pageY - $imgTop - $divHeight / 2; //selector顶点坐标 Y
        X = X < 0 ? 0 : X;
        Y = Y < 0 ? 0 : Y;
        X = X + $divWidth > $imgWidth ? $imgWidth - $divWidth : X;
        Y = Y + $divHeight > $imgHeight ? $imgHeight - $divHeight : Y;

        if ($viewImgWidth == null) {
            $viewImgWidth = $("#bigView img").outerWidth();
            $viewImgHeight = $("#bigView img").height();
            if ($viewImgWidth < 200 || $viewImgHeight < 200) {
                $viewImgWidth = $viewImgHeight = 800;
            }
            $height = $divHeight * $viewImgHeight / $imgHeight;
            $("#bigView").width($divWidth * $viewImgWidth / $imgWidth);
            $("#bigView").height($height);
        }
        var scrollX = X * $viewImgWidth / $imgWidth;
        var scrollY = Y * $viewImgHeight / $imgHeight;
        $("#bigView img").css({ "left": scrollX * -1, "top": scrollY * -1 });
        $("#bigView").css({ "top": 75, "left": $(".preview").offset().left + $(".preview").width() + 15 });

        return { left: X, top: Y };
    }
});

(function(){
    $("#btnIntro").click(function(){
        $("#introduction").show();
        $("#param").hide();
        //$(this).toggleClass("btnred");
        //$("#btnParam").toggleClass("btnred");

    }).click();
    $("#btnParam").click(function(){
        $("#param").show();
        $("#introduction").hide();
    });
})();

//减数量
function subNum(){
    var num = parseInt($("#proinfo_quantity").val().trim());
    if(isNaN(num)){
        num=1;
    }else{
        num--;
    }
    if(num<=0){
        num=1;
    }
    $("#proinfo_quantity").val(num);
}
//增加数量
function addNum(){
    var num = parseInt($("#proinfo_quantity").val().trim());
    if(isNaN(num)){
        num=1;
    }else{
        num++;
    }
    $("#proinfo_quantity").val(num);
}
/******************页面加载查询库存************************/
$(function(){
    getProvince(callProvice);
});
function callProvice(arr){
    for(var i=0;i<arr.length;i++){
        proid = arr[0].id;
        if(localCity==arr[i].name){
            tempId = arr[i].id;
            $("#province-panel").html(localCity);
            $("#addprovince").append('<div class="item currentcity" data-pid='+arr[i].id+'>'+arr[i].name+'</div>');
        }else{
            $("#addprovince").append('<div class="item" data-pid='+arr[i].id+'>'+arr[i].name+'</div>');
        }
    }
    getCity(callCity, proid);
}
function callCity(arr){
    for(var i=0;i<arr.length;i++){
        cityid = arr[0].id;
        if(i == 0){
            $("#city-panel").html(arr[i].name);
            $("#addcity").append('<div class="item currentcity" data-cid='+arr[i].id+'>'+arr[i].name+'</div>');
        }else{
            $("#addcity").append('<div class="item" data-cid='+arr[i].id+'>'+arr[i].name+'</div>');
        }
    }
    getCounty(callCounty,cityid);
}
function callCounty(arr){
    $("#addcountry").html("");
        for(var i=0;i<arr.length;i++){
            countryid = arr[0].id;
            if(i == 0){
                $("#country-panel").html(arr[i].name);
                $("#addcountry").append('<div class="item currentcity" data-counid='+arr[i].id+'>'+arr[i].name+'</div>');
            }else{
                $("#addcountry").append('<div class="item" data-counid='+arr[i].id+'>'+arr[i].name+'</div>');
            }
        }
    var area=proid+"_"+cityid+"_"+countryid;
    checkStock(area);
}

/*****************加载地址************************/

function addCity(proid){
	var tempId = 0;
	$.ajax({
            url: "/index/getCityList",
            type: "post",
            dataType: "json",
            data: {"pid": proid},
            success: function (res) {
        		$("#addcity").html("");
                if (res.data) {
                    var arr=res.data;
                    for(var i=0;i<arr.length;i++){
                		if(localCity==arr[i].name){
                			tempId = arr[i].id;
                    		$("#city-panel").html(localCity);
                    		$("#addcity").append('<div class="item currentcity" data-cid='+arr[i].id+'>'+arr[i].name+'</div>');
                    	}else{
                    		$("#addcity").append('<div class="item" data-cid='+arr[i].id+'>'+arr[i].name+'</div>');
                    	}
                    }
                }
            }
        });
    return tempId;
 }
function addDistrict(cityid){
 	var did = 0;
     $.ajax({
                url: "/index/getCountyList",
                type: "post",
                dataType: "json",
                data: {"cid": cityid},
                success: function (res) {
                    $("#addcountry").html("");
                    if (res.data) {
                        var arr=res.data;
                        for(var i=0;i<arr.length;i++){
                        	if(localDistrict==arr[i].name){
                        		did = arr[i].id;
	                    		$("#country-panel").html(localDistrict);
	                    		$("#addcountry").append('<div class="item currentcity" data-counid='+arr[i].id+'>'+arr[i].name+'</div>');
	                    	}else{
	                    		$("#addcountry").append('<div class="item" data-counid='+arr[i].id+'>'+arr[i].name+'</div>');
	                    	}
                        }
                    }
                }
            });
        return did;
 }
$(function(){
        var pid=0;
        var cid=0;
        var counid=0;
        hoverflag = 0;//滑过隐藏标志

        //选择某个省份动态加载该省份的城市列表
        $(document).on("click","#addprovince .item",function(){
        	$("#province-panel").html($(this).html());
        	$("#city-panel").html("");
            hoverflag = undefined ;
        	$(this).addClass("currentcity");
            $(this).siblings().removeClass("currentcity");
            pid=$(this).data("pid");
            $("#city-panel").click();
            $.ajax({
                url: "/index/getCityList",
                type: "post",
                dataType: "json",
                data: {"pid": pid},
                success: function (res) {
                    $("#addcity").html("");
                    if (res.data) {
                        var arr=res.data;
                        for(var i=0;i<arr.length;i++){
                                $("#addcity").append('<div class="item" data-cid=' + arr[i].id + '>' + arr[i].name + '</div>');
                        }
                    }
                }
            });
        });

        $(document).on("click","#addcity .item",function(){
        	$("#city-panel").html($(this).html());
        	$("#country-panel").html("");
            $(this).addClass("currentcity");
            $(this).siblings().removeClass("currentcity");
            cid=$(this).data("cid");
            $("#country-panel").click();
            $.ajax({
                url: "/index/getCountyList",
                type: "post",
                dataType: "json",
                data: {"cid": cid},
                success: function (res) {
                    $("#addcountry").html("");
                    if (res.data) {
                        var arr=res.data;
                        for(var i=0;i<arr.length;i++){
                            $("#addcountry").append('<div class="item" data-counid='+arr[i].id+'>'+arr[i].name+'</div>');
                        }
                    }
                }
            });
        });
        $(document).on("click","#addcountry .item",function(){
        	$("#country-panel").html($(this).html());
            $(this).addClass("currentcity");
            $(this).siblings().removeClass("currentcity");
            counid=$(this).data("counid");
            $(".address-panel").hide();
            var area=pid+"_"+cid+"_"+counid;
            checkStock(area);
        });
    //判断库存面板
    $(".address-panel,.address-tit,.blankbg").hover(function(){
        $(".address-panel").show();
        $(".blankbg").css("border-bottom","none");
    },function(){
        if(hoverflag == 0){
            $(".address-panel").hide();
            $(".blankbg").css("border-bottom","1px solid #dfdfdf");
        }
    });
    });
function checkStock(area){
    $("#txtInStock").html("");
    var thirdcode=$('#thirdcode').data("thirdcode");
    var num = new Number($("#proinfo_quantity").val());
    if(isNaN(num)){
        num = 1;
    }
    var productinfo=thirdcode+":"+num;
    if(!area || !productinfo){
        return;
    }
    $("#addressStr").html($("#province-panel").html()+$("#city-panel").html()+$("#country-panel").html()+'<i class="glyphicon glyphicon-chevron-down" style="float: right;"></i>');
    $.ajax({
        url: "/product/checkProductStock?area="+area+"&productsinfo="+productinfo,
        type: "get",
        dataType: "json",
        success: function (res) {
            if(res.result){
                $("#txtInStock").html("有货");
                $(".invalid").show();
                $(".noinvalid").hide();
                return;
            }else{
                $("#txtInStock").html("无货");
                $(".invalid").hide();
                $(".noinvalid").show();
                return;
            }
            //$("#txtInStock").html("");
        }
    });
}

/*****************************************地区查询库存样式****************************************/


$(function() {
    $("#btnIntro").click(function () {
        var _this = $(this);
        $("#introduction").show();
        $("#param").hide();
        $("#btnParam").children().removeClass("intro");
        _this.css("color", "red");
        _this.siblings().css("color", "#000");
        _this.children().addClass("intro");
    });
    $("#btnParam").click(function () {
        var _this = $(this);
        $("#param").show();
        $("#introduction").hide();
        $("#btnIntro").children().removeClass("intro");
        _this.children().addClass("intro");
        _this.css("color", "red");
        _this.siblings().css("color", "#000");
    });

   /* $("#btnIntro").click(function () {
    });
    $("#btnParam").click(function () {

    });*/

    var cwidth=$("#introduction").width();
    $("#introduction img").each(function () {//div1下的img宽度、高度设置
        DrawImage(this, cwidth);
    });
});

function DrawImage(ImgD, FitWidth) {//下面是判断，可自己修改条件
    var image = new Image();
    image.src = ImgD.src;
    if (image.width > FitWidth) {
        ImgD.width = FitWidth;
    }
}

//减数量
function subNum(){
    var num = parseInt($("#proinfo_quantity").val().trim());
    if(isNaN(num)){
        num=1;
    }else{
        num--;
    }
    if(num==0){
        num=1;
    }
    $("#proinfo_quantity").val(num);
}
//增加数量
function addNum(){
    var num = parseInt($("#proinfo_quantity").val().trim());
    if(isNaN(num) || num<1){
        num=1;
    }else{
        num++;
    }
    $("#proinfo_quantity").val(num);
}
function closepanel(){
    $('#panelAddr').hide();
}
//数量框输入判断
function checkneg(e){
    var quantity = $(e).val().trim();
    if(quantity.length>0){
        quantity= parseInt(quantity);
        if(isNaN(quantity)){
            $(e).val("");
        }else{
            $(e).val(quantity);
        }
    }else{
        $(e).val("");
    }
}
