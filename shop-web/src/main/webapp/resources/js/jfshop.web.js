/**
 * Created by Dan on 2015/9/11.
 */

/** 积分商城前台页面公共引用 */
/*var registerurl = "http://10.120.131.184:801/NewUser/RegisterFinal.aspx";  //用户注册地址
var loginurl = "http://10.120.131.184:801/Authorization.aspx";  //用户登录地址*/
//var registerurl = "/testlogin";  //用户注册地址
//var loginurl = "/testlogin";  //用户登录地址

var mycarturl = "";
//var mycarturl = "";

var getProvince = function(callback){
    $.ajax({
        url: "/index/getProvinceList",
        type: "post",
        dataType: "json",
        success:function(res){
            if (res.data) {
                var arr=res.data;
                if(typeof (callback) !="undefined" && typeof(callback) == "function"){
                    callback(arr);
                }
            }
        }
    });
}


var getCity = function(callback, pid){
    $.ajax({
        url: "/index/getCityList?pid="+pid,
        type: "post",
        dataType: "json",
        success:function(res){
            if (res.data) {
                var arr=res.data;
                if(typeof (callback) !="undefined" && typeof(callback) == "function"){
                    callback(arr);
                }
            }
        }
    });
}

var getCounty = function(callback,cid){
    $.ajax({
        url: "/index/getCountyList?cid="+cid,
        type: "post",
        dataType: "json",
        success:function(res){
            if (res.data) {
                var arr=res.data;
                if(typeof (callback) !="undefined" && typeof(callback) == "function"){
                    callback(arr);
                }
            }
        }
    });
}

var getTown = function(callback,ctid){
    $.ajax({
        url: "/index/getTownList?id="+ctid,
        type: "post",
        dataType: "json",
        success:function(res){
            if (res.data) {
                var arr=res.data;
                if(typeof (callback) !="undefined" && typeof(callback) == "function"){
                    callback(arr);
                }
            }
        }
    });
}

/*
var showDial = function(url){
    $.dialog.open(url,{
        title:"登陆测试",width: 600, height: 300,top:'10%',fixed:true,
        padding:'20px 25px'
    });
}*/