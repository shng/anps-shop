#set($layout="/wxapp/layout/layout.vm")
<header class="mui-bar mui-bar-nav jifen-bar-nav header">
    <a href="app:type=turnback&para=no"><a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a></a>
    <h1 class="mui-title">玩转积分商城</h1>
</header>
<div class="mui-content">
    <div class="mui-col-xs-12 app-help-box">
        <p class="help-tit1">积分计划</p>
        <p class="help-tit2">
            <i>
                <img src="../../../resources/wxappimg/hp1.png">
            </i>
            积分累计
        </p>
        <p class="help-text"> <span>累积规则：</span>持个人记名卡客户在加油时，每消费1元汽油积1分，每消费2元柴油积1分。</p>
        <p class="help-text"> <span>规则说明：</span>上述为全国统一规则，各地根据本地情况，可能会在此基础上有所增加，具体请咨询中国石油当地分公司、加油站。</p>
        <p class="help-text"> <span>积分到账：</span>正常情况下积分将在您消费后的48小时之内，自动记入加油卡信息系统中的积分备用金账户。</p>
        <p class="help-text"> <span>积分时效：</span>中国石油加油卡积分的基本时效期为两年，高于同业水平；于每年6月底及12月底集中对到期的积分清零，因此您的积分实际时效期可能高于两年（例如您在2012年1月消费所获积分至2014年6月底清零，实际时效期长达两年零五个月）。</p>
        <p class="help-tit2">
            <i>
                <img src="../../../resources/wxappimg/hp2.png">
            </i>
            客户升级
        </p>
        <p class="help-text tc-red">1.客户分级</p>
        <p class="help-text">根据客户加油卡累计积分数值设置客户级别，具体分为四级：</p>
        <p class="jibie">
            <span>标准卡客户</span>
            <span class="mui-pull-right">0-5999积分</span>
        </p>
        <p class="jibie">
            <span>金卡客户&#12288;</span>
            <span class="mui-pull-right">0-5999积分</span>
        </p>
        <p class="jibie">
            <span>铂金卡客户</span>
            <span class="mui-pull-right">0-5999积分</span>
        </p>
        <p class="jibie">
            <span>钻石卡客户</span>
            <span class="mui-pull-right">0-5999积分</span>
        </p>
        <p class="help-text tc-red">2.积分乘数</p>
        <p class="help-text"> 积分乘数应用于消费积分回馈活动。</p>
        <p class="help-text"> 总部和地区设立的消费积分规则（客户加油消费累计积分的方式）均享受积分乘数，非消费的积分赠送（不与消费挂钩的积分方式）不享受积分乘数。</p>
        <p class="help-text"> 根据客户不同级别实施不同的积分乘数：</p>
        <p class="help-text"> （1）标准卡客户继续享受原有积分规则，即消费汽油100元或柴油200元，可获得100积分；</p>
        <p class="help-text"> （2）客户达到金卡级别后，消费可享受1.2倍积分乘数，即消费汽油100元或柴油200元，可获得积分120分；</p>
        <p class="help-text"> （3）客户达到铂金卡级别后，消费可享受1.5倍积分乘数，即消费汽油100元或柴油200元，可获得积分150分； </p>
        <p class="help-text"> （4）客户达到钻石卡级别后，消费可享受2倍积分乘数，即消费汽油100元或柴油200元，可获得积分200分。</p>
        <p class="help-text"> 单笔消费所获积分尾数不足1分的小数部分不作累计， 例如金卡客户消费汽油101元，计算积分乘数后为121.2分，在取整后实际计为121分。 详见下表。</p>
        <div class="help-table">
            <table width="100%">
                <thead>
                <tr class="tc-red">
                    <th>项目</th>
                    <th>升级至该级别所需累积积分</th>
                    <th>所获积分乘数</th>
                    <!--<th>标准卡</th>
                    <th>金卡</th>
                    <th>铂金卡</th>
                    <th>钻石卡</th>-->
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>标准卡</td>
                    <td>0-5999分</td>
                    <td>-</td>
                </tr>
                </tr>
                <td>金卡</td>
                <td>6000-14999分</td>
                <td>1.2</td>
                </tr>
                <tr>
                    <td>铂金卡</td>
                    <td>15000-29999分</td>
                    <td>1.5</td>
                </tr>
                <tr>
                    <td>钻石卡</td>
                    <td>30000分及以上</td>
                    <td>2</td>
                </tr>
                </tbody>
            </table>
        </div>
        <p class="help-text tc-red">3.	升级基准</p>
        <p class="help-text"> 在2013年1月1日客户分级回馈活动启动后，根据客户最近36个月内的消费累计积分进行升级；</p>
        <p class="help-text"> 相应级别的客户享受相应的积分乘数。在2013年1月之前客户获得的积分不享受积分乘。 </p>
        <p class="help-text tc-red">4.	升级时间</p>
        <p class="help-text"> 实行按月升级，即根据客户加油卡截至上月最后一天的消费地加油站日结数据， 在下月1日由系统自动统计客户消费积分累计情况，于下月5日24点对达到升级标准的客户进行统一升级。 </p>
        <p class="help-text tc-red">5.	其他注意事项：</p>
        <p class="help-text"> （1）未达到升级标准不能升级为高一级客户；同时，客户级别不可转让。</p>
        <p class="help-text"> （2）我公司保留根据国家法律、规定修改本规则的权利，并及时通告用户；条款修定自通告中声明日期或自通告发布之日起执行。修订后的条款对用户具有同等约束力。</p>
    </div>
</div>