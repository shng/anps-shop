package com.kld.shop.controller.webController;

import com.kld.shop.controller.base.BaseWebController;
import com.kld.sys.api.ISysDictService;
import com.kld.sys.po.SysDict;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dan on 2015/8/6.
 */
@Controller
@RequestMapping("dict")
public class DictWebController extends BaseWebController {

    @Resource
    private ISysDictService sys_dictService;

    @ResponseBody()
    @RequestMapping("getDictList")
    public List<SysDict> getDictList(){
        String alias = request.getParameter("alias");
        Map<String,Object> map = new HashMap<String,Object>();
        if(StringUtils.isNotBlank(alias)){
            map.put("alias", alias);
        }
        return sys_dictService.getSysDictList(map);
    }

}
