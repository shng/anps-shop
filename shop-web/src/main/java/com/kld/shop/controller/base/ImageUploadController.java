package com.kld.shop.controller.base;

import com.baidu.ueditor.ActionEnter;
import com.kld.common.util.CreateFileName;
import com.kld.common.util.FileOperateUtil;
import com.kld.common.util.StringUtil;
import com.kld.shop.allocation.Constant;
import org.csource.upload.UploadFileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by anpushang on 2016/3/17.
 * 上传公共controller
 */
@Controller
public class ImageUploadController {

    private static final Logger logger = LoggerFactory.getLogger(ImageUploadController.class);


    @RequestMapping("/plugins/ueditor/jsp/imageUp")
    public void uploadImg(HttpServletRequest request, HttpServletResponse response, String action) {

        if (action.equals("uploadimage")) {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            // 获取file框的
            Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
            // 遍历获取的所有文件
            List<String> picUrlS = new ArrayList<String>();
            String path = request.getSession().getServletContext().getRealPath("upload/product");
            for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
                try {
                    MultipartFile mf = entity.getValue();
                    String fileName = "";
                    String fileUrl = "";
                    if ("".equals(mf.getOriginalFilename())) {
                        fileName = "default.png";
                        fileUrl = "/upload/product/" + fileName;
                    }else{
                        String ext = mf.getOriginalFilename().substring(mf.getOriginalFilename().lastIndexOf("."));
                        fileName = CreateFileName.getFileName() + ext;
                        File targetFile = new File(path, fileName);
                        if (!targetFile.exists()) {
                            targetFile.mkdirs();
                        }
                        //保存文件
                        mf.transferTo(targetFile);
                        fileUrl = "/upload/product/" + fileName;
                    }

                    // 判断文件名是否为空。为空set null值
//                    String myfileUrl = UploadFileUtil.uploadFile(
//                            mf.getInputStream(),
//                            StringUtil.getFileExt2(mf.getOriginalFilename()), null);
                    if (null != fileUrl) {
                        String newUrl = Constant.IMAGE_URL01 + fileUrl;
                        picUrlS.add(Constant.IMAGE_URL01 + fileUrl);
                        String status = "{\"state\": \"SUCCESS\",\"title\": \"\",\"original\":\"" + mf.getOriginalFilename() + "\",\"type\": \"" + StringUtil.getFileExt2(fileUrl) + "\",\"url\":\"" + newUrl
                                + "\",\"size\": \"" + mf.getSize() + "\"}";
                        response.getWriter().print(status);
                        logger.info("uedtior上传图片，图片图片服务器返回 ：" + fileUrl + ";原始文件名：" + mf.getOriginalFilename());
                    } else {
                        response.getWriter().print("{\"state\": \"Server is Error!\"}");
                    }
                } catch (Exception e) {
                    try {
                        response.getWriter().print("{\"state\": \"Server is Error!\"}");
                    } catch (IOException e1) {
                        logger.error("富文本编辑器上传图片失败！！！" + e.getMessage(), e);
                    }
                    logger.error("富文本编辑器上传图片失败！！！" + e.getMessage(), e);
                }
            }
        }
    }


    /**
     * 上传单张图片
     *
     * @param request
     * @param response
     */
    @RequestMapping("/item/imageUp")
    public void uploadImages(HttpServletRequest request, HttpServletResponse response) {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        // 获取file框的
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            try {
                MultipartFile mf = entity.getValue();
                // 判断文件名是否为空。为空set null值
                String myfileUrl = UploadFileUtil.uploadFile(
                        mf.getInputStream(),
                        StringUtil.getFileExt2(mf.getOriginalFilename()), null);
                String newUrl = Constant.IMAGE_URL01 + myfileUrl;
                if (null != myfileUrl) {

                    logger.info("图片上传成功ImageUrl：" + newUrl);
                    response.getWriter().write(
                            "{\"success\":\"true\"," + "\"data\":{\"BaseUrl\":\""
                                    + myfileUrl + "\",\"Url\":\"" + newUrl
                                    + "\",\"filename\":\""
                                    + mf.getOriginalFilename() + "\"}}");
                } else {
                    logger.error("图片服务器上传图片失败！！");
                    response.getWriter().write(
                            "{\"success\":\"flase\"," + "\"data\":{\"BaseUrl\":\"\",\"Url\":\"\",\"filename\":\""
                                    + mf.getOriginalFilename() + "\"}}");

                }
            } catch (Exception e) {
                try {
                    response.getWriter().write(
                            "{\"success\":\"false\","
                                    + "\"data\":{\"BaseUrl\":\"" + ""
                                    + "\",\"Url\":\"" + ""
                                    + "\",\"filename\":\"" + "" + "\"}}");
                } catch (IOException e1) {
                    logger.error("展示属性图片上传失败！！！" + e1.getMessage(), e1);
                }
                logger.error("展示属性图片上传失败！！！" + e.getMessage(), e);
            }
        }
    }

}
