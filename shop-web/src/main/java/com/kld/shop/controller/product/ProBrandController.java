package com.kld.shop.controller.product;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kld.common.framework.dto.ProductNode;
import com.kld.common.framework.dto.ResultMsg;
import com.kld.common.framework.page.PageInfoResult;
import com.kld.product.api.IProBrandService;
import com.kld.product.api.IProCateBrandRelService;
import com.kld.product.po.ProBrand;
import com.kld.product.po.ProCategory;
import com.kld.shop.allocation.Constant;
import com.kld.shop.controller.base.BaseController;
import net.sf.json.JSONArray;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anpushang on 2016/3/28.
 */
@Controller
@RequestMapping("/admin/brand")
public class ProBrandController extends BaseController{


    @Autowired
    private IProBrandService proBrandService;
    @Autowired
    private IProCateBrandRelService proCateBrandRelService;


    /***
     * 进入列表页面
     * @return
     */
    @RequestMapping(value = "/toList")
    public String toList(){
        return "/admin/brand/brand_list";
    }


    /***
     * 品牌列表查询
     * @param proBrand
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/list")
    public ResultMsg list(ProBrand proBrand){
        PageInfo pageInfo = null;
        try{
            proBrand.setIsDel(Constant.ENABLE);
            pageInfo = proBrandService.findProBrandListPage(getPageNum(),getPageSize(),proBrand);
        }catch (Exception e){
            logger.error("品牌查询"+e.getMessage(),e);
            e.printStackTrace();
        }
        return PageInfoResult.PageInfoMsg(pageInfo);
    }


    /***
     * 品牌删除 逻辑删除
     * @param id
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/delBrandById")
    public ResultMsg delBrandById(Integer id){
        ProBrand proBrand = new ProBrand();
        proBrand.setIsDel(Constant.DISABLE);
        proBrand.setId(id);
        ResultMsg resultMsg = new ResultMsg();
        try{
            int resultCount = proBrandService.updateByPrimaryKey(proBrand);
            resultMsg = resultCount > 0 ? successMsg(resultMsg,"删除成功") : errorMsg(resultMsg,"删除失败");
        }catch (Exception e){
            logger.error("品牌删除"+e.getMessage(),e);
            e.printStackTrace();
        }
        return resultMsg;
    }


    /***
     * 品牌更新
     * @param proBrand
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/upBrandById")
    public ResultMsg updateBrandById(ProBrand proBrand){
        ResultMsg resultMsg = new ResultMsg();
        try{
            int resultCount = proBrandService.updateByPrimaryKey(proBrand);
            resultMsg = resultCount > 0 ? successMsg(resultMsg,"更新成功") : errorMsg(resultMsg,"更新失败");
        }catch (Exception e){
            logger.error("品牌更新"+e.getMessage(),e);
            e.printStackTrace();
        }
        return resultMsg;
    }

    /***
     * 品牌新增
     * @param proBrand
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/saveBrand")
    public ResultMsg saveBrand(ProBrand proBrand){
        ResultMsg resultMsg = new ResultMsg();
        try{
            proBrand.setIsDel(Constant.ENABLE);
            if (null != proBrand.getId()){
                int resultCount = proBrandService.updateByPrimaryKey(proBrand);
                resultMsg = resultCount > 0 ? successMsg(resultMsg,Constant.UPDATE_SUCCESS) : errorMsg(resultMsg,Constant.UPDATE_SUCCESS);
            }else{
                int resultCount = proBrandService.insert(proBrand);
                resultMsg = resultCount > 0 ? successMsg(resultMsg,Constant.INSERT_SUCCESS) : errorMsg(resultMsg,Constant.INSERT_ERROR);
            }
        }catch (Exception e){
            logger.error("品牌新增"+e.getMessage(),e);
            e.printStackTrace();
        }
        return resultMsg;
    }



    /***
     * 根据品牌ID查询信息
     * @param id
     * @return
     */
    @ResponseBody()
    @RequestMapping(value = "/getBrandById")
    public ResultMsg getBrandById(Integer id){
        ResultMsg resultMsg = new ResultMsg();
        try{
            ProBrand proBrand = proBrandService.selectByPrimaryKey(id);
            resultMsg.setData(proBrand);
        }catch (Exception e){
            logger.error("品牌查询根据ID"+e.getMessage(),e);
            e.printStackTrace();
        }
        return resultMsg;
    }

    /***
     * 根据分类ID查询品牌信息
     * @param cateId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getBrandByCateId")
    public String getBrandByCateId(Integer cateId){
        String jsonStr = "";
        try{
            List<ProBrand> proBrandList = proCateBrandRelService.getBrandByCateId(cateId);
            List<ProductNode> productNodeList = new ArrayList<ProductNode>();
            for (ProBrand proBrand : proBrandList) {
                ProductNode productNode = new ProductNode();
                productNode.setId(String.valueOf(proBrand.getId()));
                productNode.setText(proBrand.getBrandName());
                productNodeList.add(productNode);
            }
            JSONArray jsonArray = JSONArray.fromObject(productNodeList);
            jsonStr = String.format("%s", JSON.toJSONString(jsonArray));
        }catch (Exception e){
            logger.error("根据分类ID查询品牌信息"+e.getMessage(),e);
            e.printStackTrace();
        }
        return jsonStr;
    }

}
