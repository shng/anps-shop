package com.kld.shop.controller.product;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by hasee on 2016/3/30.
 */
@Controller
@RequestMapping("/view")
public class ViewTestController {

    @RequestMapping("/test1")
    public String test1(){
        return "/index";
    }
    @RequestMapping("/test2")
    public String test2(){
        return "/prolist";
    }
    @RequestMapping("/test3")
    public String test3(){
        return "/proinfo";
    }
}
