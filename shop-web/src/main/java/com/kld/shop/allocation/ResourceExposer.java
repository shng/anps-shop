package com.kld.shop.allocation;

import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;

/**
 * Created by anpushang on 2016/3/21.
 * 通过Spring框架在ServletContext层面注入静态资源根路径信息
*/
public class ResourceExposer implements ServletContextAware {


    private String imageRoot;

    private String cssRoot;

    private String jsRoot;

    private String pluginsRoot;

    private String fdfsBigRoot;

    private String fdfsMiddleRoot;

    private String fdfsSmallRoot;

    private ServletContext application;

    @Override
    public void setServletContext(ServletContext servletContext) {
        application = servletContext;
    }

    public ServletContext getServletContext() {
        return this.application;
    }

    public void init() throws Exception {
        getServletContext().setAttribute("imageRoot",imageRoot);
        getServletContext().setAttribute("cssRoot",cssRoot);
        getServletContext().setAttribute("jsRoot",jsRoot);
        getServletContext().setAttribute("pluginsRoot",pluginsRoot);
        getServletContext().setAttribute("fdfsBigRoot",fdfsBigRoot);
        getServletContext().setAttribute("fdfsMiddleRoot",fdfsMiddleRoot);
        getServletContext().setAttribute("fdfsSmallRoot",fdfsSmallRoot);
    }


    public String getImageRoot() {
        return imageRoot;
    }

    public void setImageRoot(String imageRoot) {
        this.imageRoot = imageRoot;
    }

    public String getCssRoot() {
        return cssRoot;
    }

    public void setCssRoot(String cssRoot) {
        this.cssRoot = cssRoot;
    }

    public String getJsRoot() {
        return jsRoot;
    }

    public void setJsRoot(String jsRoot) {
        this.jsRoot = jsRoot;
    }

    public String getPluginsRoot() {
        return pluginsRoot;
    }

    public void setPluginsRoot(String pluginsRoot) {
        this.pluginsRoot = pluginsRoot;
    }

    public String getFdfsBigRoot() {
        return fdfsBigRoot;
    }

    public void setFdfsBigRoot(String fdfsBigRoot) {
        this.fdfsBigRoot = fdfsBigRoot;
    }

    public String getFdfsMiddleRoot() {
        return fdfsMiddleRoot;
    }

    public void setFdfsMiddleRoot(String fdfsMiddleRoot) {
        this.fdfsMiddleRoot = fdfsMiddleRoot;
    }

    public String getFdfsSmallRoot() {
        return fdfsSmallRoot;
    }

    public void setFdfsSmallRoot(String fdfsSmallRoot) {
        this.fdfsSmallRoot = fdfsSmallRoot;
    }
}
