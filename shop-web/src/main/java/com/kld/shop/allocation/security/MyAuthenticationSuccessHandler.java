/**
 * 58.com Inc.
 * Copyright (c) 2005-2015 All Rights Reserved.
 */
package com.kld.shop.allocation.security;

//old import。
import com.kld.common.util.MD5;
import com.kld.sys.api.ISysUserService;
import com.kld.sys.po.SysUser;
import com.kld.shop.controller.base.BaseController;


import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * 
 * @author yangjian
 * @version $Id: MyAuthenticationSuccessHandler.java, v 0.1 2015-7-10 上午1:23:27 yangjian Exp $
 */
public class MyAuthenticationSuccessHandler implements  AuthenticationSuccessHandler{

    static final Log LOG = LogFactory.getLog(MyAuthenticationSuccessHandler.class.getName());
    
    @Resource
    private ISysUserService userService;
    /** 
     * @see AuthenticationSuccessHandler#onAuthenticationSuccess(HttpServletRequest, HttpServletResponse, Authentication)
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException,
                                                                      ServletException {
            String ru = (String)request.getSession().getAttribute("ru");  
            request.getSession().removeAttribute("ru"); 
            if(StringUtils.isEmpty(ru)){  
                ru = "/admin/home/index";
            }
            
            String userName = BaseController.getUserDetails().getUsername();
            SysUser user = userService.selectUserMoreInfo(userName);
            request.getSession(true).setAttribute("username", userName);
            request.getSession(true).setAttribute("rname", user.getRealname());
            request.getSession().setAttribute("oucode", user.getOucode());
            request.getSession(true).setAttribute("ouname", user.getOuname());
            request.getSession(true).setAttribute("oulevel", user.getOulevel());
            String authorized = MD5.encodeByMd5AndSalt(userName + "@jifen@ssologin");
            Cookie c = new Cookie("CookieAuthorize", "userid="+userName+"&authorized="+authorized);
            c.setMaxAge( 60 * 60);
            response.addCookie(c);
            response.sendRedirect(ru);
    }  
}
