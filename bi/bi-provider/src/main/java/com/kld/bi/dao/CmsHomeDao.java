package com.kld.bi.dao;


import com.kld.bi.po.CmsHome;
import com.kld.common.framework.dao.impl.SimpleDaoImpl;
import org.springframework.stereotype.Repository;

/**
 * Created by anpushang on 2016/3/23.
 */
@Repository
public class CmsHomeDao extends SimpleDaoImpl<CmsHome> {
}
