package com.kld.bi.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by 曹不正 on 2016/3/24.
 */
public class Report implements Serializable {
    private static final long serialVersionUID = 5133614305804304395L;
    private BigDecimal id;

    private Date rptdate;

    private BigDecimal type;    //报告类型（1-积分兑换；2积分抽奖；3供应商统计）

    private BigDecimal totalnum;

    private BigDecimal totalpoints;

    private BigDecimal totalprice;

    private BigDecimal tax;

    private String oucode;

    private String mctcode;

    private Date createtime;

    private String creator;

    private  BigDecimal freight;

    public BigDecimal getFreight() {
        return freight;
    }

    public void setFreight(BigDecimal freight) {
        this.freight = freight;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Date getRptdate() {
        return rptdate;
    }

    public void setRptdate(Date rptdate) {
        this.rptdate = rptdate;
    }

    public BigDecimal getType() {
        return type;
    }

    public void setType(BigDecimal type) {
        this.type = type;
    }

    public BigDecimal getTotalnum() {
        return totalnum;
    }

    public void setTotalnum(BigDecimal totalnum) {
        this.totalnum = totalnum;
    }

    public BigDecimal getTotalpoints() {
        return totalpoints;
    }

    public void setTotalpoints(BigDecimal totalpoints) {
        this.totalpoints = totalpoints;
    }

    public BigDecimal getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(BigDecimal totalprice) {
        this.totalprice = totalprice;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public String getOucode() {
        return oucode;
    }

    public void setOucode(String oucode) {
        this.oucode = oucode == null ? null : oucode.trim();
    }

    public String getMctcode() {
        return mctcode;
    }

    public void setMctcode(String mctcode) {
        this.mctcode = mctcode == null ? null : mctcode.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

}
