package com.kld.third.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.kld.third.api.jingdong.IJdBaseService;
import com.kld.third.api.jingdong.IJdOrderService;
import com.kld.third.dto.jingdong.JDOrderTrack;
import com.kld.third.dto.jingdong.JDPOrder;
import com.kld.third.dto.jingdong.constant.JDConstant;
import net.sf.json.JSON;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kwg on 2016/3/28.
 */
@Service
public class JdOrderServiceImpl implements IJdOrderService {

    @Autowired
    private IJdBaseService jdBaseService;

    @Override
    public String createOrder(JDPOrder jdpOrder) {
        String dataParams = "&thirdOrder=" + jdpOrder.getThirdOrder() + "&sku=" + jdpOrder.getSku()
                + "&name=" + jdpOrder.getName() + "&province=" + jdpOrder.getProvince() + "&city=" + jdpOrder.getCity()
                + "&county=" + jdpOrder.getCounty() + "&town=" + jdpOrder.getTown() + "&address=" + jdpOrder.getAddress()
                + "&zip=" + jdpOrder.getZip() + "&phone=" + jdpOrder.getPhone() + "&mobile=" + jdpOrder.getMobile()
                + "&email=" + jdpOrder.getEmail() + "&remark=" + jdpOrder.getRemark() + "&invoiceState=" + jdpOrder.getInvoiceState()
                + "&invoiceType=" + jdpOrder.getInvoiceType() + "&selectedInvoiceTitle=" + jdpOrder.getSelectedInvoiceTitle() + "&companyName=" + jdpOrder.getCompanyName()
                + "&invoiceContent=" + jdpOrder.getInvoiceContent() + "&paymentType=" + jdpOrder.getPaymentType() + "&submitState=" + jdpOrder.getSubmitState()
                + "&invoiceName=" + jdpOrder.getInvoiceName() + "&invoicePhone=" + jdpOrder.getInvoicePhone() + "&invoiceProvice=" + jdpOrder.getInvoiceProvice()
                + "&invoiceCity=" + jdpOrder.getCity() + "&invoiceCounty=" + jdpOrder.getInvoiceCounty() + "&invoiceAddress=" +jdpOrder.getInvoiceAddress();
        return jdBaseService.returnMsg(JDConstant.URL_ORDER_SUBMIT,dataParams);
    }

    @Override
    public boolean confirmOrder(String jDOrderId) {
        String dataParams = "&jdOrderId=" + jDOrderId;
        String res = jdBaseService.returnMsg(JDConstant.URL_ORDER_COMFIRM,dataParams);
        JSONObject jso = jdBaseService.returnMsg(res);
        return jso.getBoolean("success")?jso.getBoolean("result"):false;
    }

    @Override
    public boolean cancelOrder(String jDOrderId) {
        String dataParams = "&jdOrderId=" +  jDOrderId;
        String res = jdBaseService.returnMsg(JDConstant.URL_ORDER_CANCEL,dataParams);
        JSONObject jso = jdBaseService.returnMsg(res);
        return jso.getBoolean("success")?jso.getBoolean("result"):false;
    }

    @Override
    public JDPOrder getOrderDetail(String jDOrderId) {
        String dataParams = "&jdOrderId=" + jDOrderId;
        String res =  jdBaseService.returnMsg(JDConstant.URL_ORDER_SELECTJDORDER, dataParams);
        JSONObject jso = jdBaseService.returnMsg(res);
        if(jso.getBoolean("success")){
            String jsoRes = jso.getString("result");
            return com.alibaba.fastjson.JSON.parseObject(jsoRes,JDPOrder.class);
        }
        return null;
    }

    @Override
    public String getJdOrderIdByOrderId(String orderid) {
        String dataParams = "&thirdOrder=" + orderid;
        String res = jdBaseService.returnMsg(JDConstant.URL_ORDER_SELECTJDORDERID_BYTHIRDORDER,dataParams);
        JSONObject jso = jdBaseService.returnMsg(res);
        return jso.getBoolean("success")? jso.getString("result"):"";
    }

    @Override
    public JDPOrder getJdOrderByOrderId(String orderid) {
        String jid = getJdOrderIdByOrderId(orderid);
        return getOrderDetail(jid);
    }

    @Override
    public List<JDOrderTrack> getJdOrderTrackByJdOrderId(String jDOrderId) {
        List<JDOrderTrack> orderTracks = new ArrayList<JDOrderTrack>();
        String dataParams = "&jdOrderId=" + jDOrderId;
        String res = jdBaseService.returnMsg(JDConstant.URL_ORDER_TRACK, dataParams);
        JSONObject jso = jdBaseService.returnMsg(res);
        if(jso.getBoolean("success")){
            return com.alibaba.fastjson.JSON.parseArray(jso.getString("reslut"),JDOrderTrack.class);
        }
        return orderTracks;
    }
}
