package com.kld.third.api.jingdong;

import com.alibaba.fastjson.JSONObject;
import com.kld.third.dto.jingdong.JDSku;
import com.kld.third.dto.jingdong.JDSkuStockState;

import java.util.List;

/**
 * Created by kwg on 2016/3/28.
 */
public interface IJdProductService {

    public List<String> getProPoolNums();   //批量获取商品池编码
    public String getSkuIds(String proPoolNum);  //根据商品池批量获取商品SKUID
    public JDSku getProDetail(String skuId);//根据SKUID查询商品详情
    public  List<JDSku> getProState(String skuIds) ; // //查询商品上下架状态(批量查询,逗号分隔)
    public  List<net.sf.json.JSONObject> getProImageList(String skuIds); ////查询商品图片列表(批量查询,逗号分隔)
    public  List<JDSku>  getProAreaLimit(String skuIds,Integer provinceid,Integer cityid,Integer countyid,Integer townid); //批量查询商品区域购买限制

    /*===================商品库存相关=============================*/

    /**
     * 判断单个商品是否有库存(适用于商品详情、下单检查库存)
     * @param skuId 第三方商品SKUID
     * @param num  购买数量
     * @param provinceid 省份ID
     * @param cityid 城市id
     * @param countyid 区/县id
     * @return
     */
    public boolean hasStock(String skuId, Integer num, Integer provinceid,Integer cityid,Integer countyid);


    /**
     * 6.2 批量获取库存接口（建议订单详情页、下单使用）
     * @param skuNums  商品和数量  [{skuId: 569172,num:101}]
     * @param provinceid 省份id
     * @param cityid 城市id
     * @param countyid 区/县id
     * @return
     */
    public  List<JDSkuStockState> getStockStateWithNum(String skuNums,Integer provinceid,Integer cityid,Integer countyid);//批量查询商品库存状态（带库存剩余数量，适用于商品详情、下单） remainNum 剩余数量 -1未知；当库存小于5时展示真实库存数量

    /**
     * 6.2 批量获取库存接口（建议商品列表页使用）
     * @param skuIds  商品第三方商品编码，批量查询逗号分隔
     * @param provinceid 省份id
     * @param cityid 城市id
     * @param countyid 区/县id
     * @return
     */
    public List<JDSkuStockState> getStockState(String skuIds,Integer provinceid,Integer cityid,Integer countyid);//批量查询商品库存状态（不带库存剩余数量，适用于列表）

    /*===========================================================*/

    /***********************商品价格相关************************************/
    public List<JDSku> getPrice(String skuIds); //批量获取京东价格

    /***************************************************************/


}
