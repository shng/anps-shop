package com.kld.third.dto.jingdong;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kwg on 2016/3/28.
 */
public class JDCOrder implements Serializable{

    private static final long serialVersionUID = 3399521271123986690L;
    private String  jdOrderId; //京东订单号
    private String pOrder; //京东父订单号
    private Integer state; // 物流状态 0 是新建 1是妥投 2是拒收
    private Integer Type; //订单类型 1是父订单 2是子订单
    private Double orderPrice; //订单价格
    private Double orderNakedPrice; //订单裸价
    private Double orderTaxPrice;//京东税价
    private List<JDSku> skus; //商品列表
    private List<JDOrderTrack> orderTrack; //配送信息

    public String getJdOrderId() {
        return jdOrderId;
    }

    public void setJdOrderId(String jdOrderId) {
        this.jdOrderId = jdOrderId;
    }

    public String getpOrder() {
        return pOrder;
    }

    public void setpOrder(String pOrder) {
        this.pOrder = pOrder;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getType() {
        return Type;
    }

    public void setType(Integer type) {
        Type = type;
    }

    public Double getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(Double orderPrice) {
        this.orderPrice = orderPrice;
    }

    public Double getOrderNakedPrice() {
        return orderNakedPrice;
    }

    public void setOrderNakedPrice(Double orderNakedPrice) {
        this.orderNakedPrice = orderNakedPrice;
    }

    public Double getOrderTaxPrice() {
        return orderTaxPrice;
    }

    public void setOrderTaxPrice(Double orderTaxPrice) {
        this.orderTaxPrice = orderTaxPrice;
    }

    public List<JDSku> getSkus() {
        return skus;
    }

    public void setSkus(List<JDSku> skus) {
        this.skus = skus;
    }

    public List<JDOrderTrack> getOrderTrack() {
        return orderTrack;
    }

    public void setOrderTrack(List<JDOrderTrack> orderTrack) {
        this.orderTrack = orderTrack;
    }
}
