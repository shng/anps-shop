package com.kld.third.api.jingdong;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by kwg on 2016/3/28.
 */
public interface IJdMessageService {

    public JSONObject getMessage(Integer type, String params, boolean del);
    public boolean delMessage(String mid);

}
