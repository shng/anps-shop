package com.kld.third.api.jingdong.cardcore;

import java.util.Map;

/**
 * Created by xiaohe on 2016/3/29.
 */
public interface ICardCoreService {
    /**
     * 调用电子充值卡接口
     * @param url
     * @param operationName
     * @param map
     * @return
     */
    public Map<String,Object> electronicCard(String url, String operationName, Map<String,Object> map);

    /**
     *
     * 调用卡系统积分
     * @param url
     * @param operationName
     * @param map
     * @return
     */
    public Map<String,Object>  integralCard( String url, String operationName,Map<String,Object> map);
}
