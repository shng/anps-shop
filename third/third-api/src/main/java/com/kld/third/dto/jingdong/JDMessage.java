package com.kld.third.dto.jingdong;

import java.io.Serializable;

/**
 * Created by kwg on 2016/3/28.
 */
public class JDMessage implements Serializable{
    private static final long serialVersionUID = -990032414025753239L;
    private String id; //京东消息ID
    private String result; //消息内容
    /**
     * -- 1代表订单拆分变更
     {"id": {"id":{"id":{"id":{"id":推送id, "result" : {"pOrderid, "result" : {"pOrder id, "result" : {"pOrderid, "result" : {"pOrder id, "result" : {"pOrder id, "result" : {"pOrderid, "result" : {"pOrderid, "result" : {"pOrderid, "result" : {"pOrderid, "result" : {"pOrder id, "result" : {"pOrderid, "result" : {"pOrderid, "result" : {"pOrderid, "result" : {"pOrder " :父订单id} , "type": 1, time":id} , "type": 1, time": id} , "type": 1, time":id} , "type": 1, time":id} , "type": 1, time": id} , "type": 1, time":id} , "type": 1, time":id} , "type": 1, time": id} , "type": 1, time":id} , "type": 1, time":id} , "type": 1, time":id} , "type": 1, time": 推送时间},
     --2代表商品协议价格、京东价格变更
     {"id":推送id, "result":{"skuId" : 商品编号 }, "type": 2, "time":推送时间},
     --5代表该订单已妥投
     {"id": {"id":{"id": {"id":推送id, "result":{"orderId":"id, "result":{"orderId":" id, "result":{"orderId":"id, "result":{"orderId":"id, "result":{"orderId":" id, "result":{"orderId":" id, "result":{"orderId":"id, "result":{"orderId":"id, "result":{"orderId":"id, "result":{"orderId":"id, "result":{"orderId":"id, "result":{"orderId":" id, "result":{"orderId":" id, "result":{"orderId":"id, "result":{"orderId":" id, "result":{"orderId":"京东订单编号", "state":"1", "state":"1 ", "state":"1", "state":"1 ", "state":"1 ", "state":"1 ", "state":"1", "state":"1 是妥投，2是拒收"}, "type" : "}, "type" : "}, "type" : "}, "type" : "}, "type" : "}, "type" : "}, "type" : "}, "type" : "}, "type" : 5, "time": 5, "time":5, "time":5, "time":5, "time": 推送时间}，
     --6代表添加、删除商品池内商品
     {"id": {"id":{"id": {"id":推送id, "result":{"skuId": id, "result":{"skuId": id, "result":{"skuId": id, "result":{"skuId": id, "result":{"skuId": id, "result":{"skuId": id, "result":{"skuId": id, "result":{"skuId": id, "result":{"skuId": id, "result":{"skuId": id, "result":{"skuId": id, "result":{"skuId": id, "result":{"skuId": id, "result":{"skuId": id, "result":{"skuId": 商品编号, "page_num": , "page_num":, "page_num": , "page_num":, "page_num":商品池编号, "state":"1, "state":"1, "state":"1, "state":"1 , "state":"1 , "state":"1 , "state":"1, "state":"1, "state":"1添加，2删除"}, "type" : 6, "time":推送时间}
     --10表示订单已被删除
     {"id":推送id, "result" : {"orderId":8239844894} , "type": 10, "time":推送时间}
     如果参数传递了type类型，则返回内容均为该type类型
     */
    private Integer type; //消息类型（1代表订单拆分变更；2代表商品协议价格、京东价格变更；5代表该订单已妥投；6代表添加、删除商品池内商品；10表示订单已被删除）
    private Integer del; //是否删除标示，1为获取完后，立刻删除，0为获取后不删除
    private String time; //推送时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
