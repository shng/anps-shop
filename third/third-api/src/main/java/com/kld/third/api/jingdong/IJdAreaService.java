package com.kld.third.api.jingdong;

import com.kld.third.dto.jingdong.JDArea;

import java.util.List;

/**
 * Created by kwg on 2016/3/28.
 */
public interface IJdAreaService {

    public List<JDArea> getProvinceList();
    public List<JDArea> getCityList(Integer provinceid);
    public List<JDArea> getCountyList(Integer cityid);
    public List<JDArea> getTownList(Integer countyid);

}
