package com.kld.third.dto.jingdong.constant;

/**
 * Created by kwg on 2016/3/28.
 */
public class JDEnums {

    /**
     * 京东订单物流状态
     */
    public static enum JDOrderState{

        UnPaied(0,"新建"),Delivered(1,"妥投"),Refused(2,"拒收");
        public int value;
        public String text;

        private JDOrderState(Integer value,String text){
            this.value = value;
            this.text = text;
        }
    }

    /**
     * 京东库存状态（有库存；没有库存）
     * 京东状态 33 有货 现货-下单立即发货
        39 有货 在途-正在内部配货，预计2~6天到达本仓库
        40 有货 可配货-下单后从有货仓库配货
        36 预订
        34 无货
     */
    public static enum JDStockState{
        Full(33,"有货"),Empty(34,"无货");
        private int value;
        private String text;

        private JDStockState(Integer value,String text){
            this.value = value;
            this.text = text;
        }

        public static String getStockStateText(Integer value){
            String text = "";
            boolean hasStock = hasStock(value);
            if(hasStock){
               text = JDStockState.Full.text;
            }else {
                text = JDStockState.Empty.text;
            }
            return text;
        }

        public static boolean hasStock(Integer value){
            boolean res = false;
            switch (value){
                case 33:
                    res = true;
                    break;
                case 39:
                    res = true;
                    break;
                case 40:
                    res = true;
                    break;
                case 36:
                    res = false;
                    break;
                case 34:
                    res = false;
                    break;
                default:
                    res = false;
                    break;
            }
            return res;
        }
    }

}
